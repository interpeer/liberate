/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <liberate/sys/pid.h>
#include <liberate/logging.h>

#if defined(LIBERATE_HAVE_GETPID)
#  include <sys/types.h>
#  include <unistd.h>
#endif

#if defined(LIBERATE_HAVE__GETPID)
#  include <process.h>
#endif


namespace liberate::sys {

int getpid()
{
#if defined(LIBERATE_HAVE_GETPID)
  return ::getpid();
#endif

#if defined(LIBERATE_HAVE__GETPID)
  return ::_getpid();
#endif

  LIBLOG_ERROR("getpid() not implemented");
  return -1;
}

} // namespace liberate::sys
