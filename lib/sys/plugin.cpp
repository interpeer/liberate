/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <liberate/sys/plugin.h>

#include <build-config.h>

#include <liberate/logging.h>

#if defined LIBERATE_HAVE_DL
#include <dlfcn.h>
#endif

#include <iostream> // FIXME

namespace liberate::sys {

auto constexpr PLUGIN_SYMBOL_NAME = "liberate_plugin_meta";

#if defined(LIBERATE_POSIX)

LIBERATE_API bool
load_plugin(plugin_meta const * & meta, std::string const & filename)
{
#if defined(LIBERATE_HAVE_DL)
  // OK, try to use dlopen.
  auto ref = dlopen(filename.c_str(), RTLD_LAZY | RTLD_LOCAL);
  if (!ref) {
    LIBLOG_ERROR("Could not load plugin: " << filename);
    return false;
  }

  // Now find the symbol
  auto symbol = dlsym(ref, PLUGIN_SYMBOL_NAME);
  if (!symbol) {
    LIBLOG_ERROR("Could not find symbol in plugin file.");
    dlclose(ref);
    return false;
  }

  // Create new meta from the loaded one.
  typedef void * (*plugin_meta_func)();
  auto symbol_func = reinterpret_cast<plugin_meta_func>(symbol);
  auto meta_copy = new plugin_meta{*static_cast<plugin_meta *>(symbol_func())};
  meta_copy->plugin_handle = ref;

  // Finish up
  meta = meta_copy;
  return true;
#else // LIBERATE_HAVE_DL
  std::cerr << "no DL found, huh." << std::endl;
  return false;
#endif // LIBERATE_HAVE_DL
}


LIBERATE_API bool
unload_plugin(plugin_meta const * & meta)
{
#if defined(LIBERATE_HAVE_DL)
  if (!meta || !meta->plugin_handle) {
    LIBLOG_ERROR("Invalid or incomplete plugin meta.");
    return false;
  }

  dlclose(meta->plugin_handle);

  delete meta;
  meta = nullptr;
  return true;
#else // LIBERATE_HAVE_DL
  return false;
#endif // LIBERATE_HAVE_DL
}

#else

namespace {

struct SetErrorModeShim
{
  inline ~SetErrorModeShim()
  {
    FreeLibrary(lib);
  }

  inline UINT operator()(UINT mode)
  {
    if (!load_func()) {
      return SetErrorMode(mode);
    }

    DWORD old_mode = 0;
    auto res = func(mode, &old_mode);
    return res ? old_mode : 0;
  }

private:
  typedef BOOL (WINAPI * func_ptr)(DWORD, DWORD *);

  inline bool load_func()
  {
    if (failed || !lib || !func) {
      return false;
    }

    // Get the thread enabled version
    lib = GetModuleHandleA("Kernel32.dll");
    if (lib) {
      func = reinterpret_cast<func_ptr>(GetProcAddress(lib, "SetThreadErrorMode"));
    }

    failed = func == nullptr;
    return !failed;
  }

  BOOL      failed = FALSE;
  func_ptr  func = nullptr;
  HMODULE   lib = nullptr;
};

} // anonymous namespace


LIBERATE_API bool
load_plugin(plugin_meta const * & meta, std::string const & filename)
{
  SetErrorModeShim SetErrorMode{};
  UINT old_mode = SetErrorMode(SEM_FAILCRITICALERRORS);

  HANDLE current_process = GetCurrentProcess();

  HMODULE library = LoadLibraryExA(filename.c_str(), nullptr,
      LOAD_WITH_ALTERED_SEARCH_PATH);
  if (!library) {
    LIBLOG_ERRNO("Could not load plugin: " << filename);
    SetErrorMode(old_mode);
    return false;
  }

  FARPROC symbol = GetProcAddress(library, PLUGIN_SYMBOL_NAME);
  if (!symbol) {
    LIBLOG_ERRNO("Could not find symbol in plugin file.");
    SetErrorMode(old_mode);
    return false;
  }

  // Create new meta from the loaded one.
  auto meta_copy = new plugin_meta{*reinterpret_cast<plugin_meta *>(symbol())};
  meta_copy->plugin_handle = library;

  // Finish up
  meta = meta_copy;
  SetErrorMode(old_mode);
  return true;
}


LIBERATE_API bool
unload_plugin(plugin_meta const * & meta)
{
  if (!meta || !meta->plugin_handle) {
    LIBLOG_ERROR("Invalid or incomplete plugin meta.");
    return false;
  }

  SetErrorModeShim SetErrorMode{};
  UINT old_mode = SetErrorMode(SEM_FAILCRITICALERRORS);

  FreeLibrary(reinterpret_cast<HMODULE>(meta->plugin_handle));

  delete meta;
  meta = nullptr;

  SetErrorMode(old_mode);
  return true;
}

#endif // LIBERATE_POSIX


LIBERATE_API bool
load_plugin(plugin_meta const * & meta, char const * filename)
{
  return load_plugin(meta, filename ? std::string{filename} : std::string{});
}

} // namespace liberate::sys
