/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <liberate/sys/memory.h>

#include <stddef.h>

#if defined(LIBERATE_HAVE_SECURE_ZERO_MEMORY)
#include <Windows.h>
#else
#include <string.h>
#endif



namespace liberate::sys {


LIBERATE_API void
secure_memset(void * buffer, size_t size, char value)
{
#if defined(LIBERATE_HAVE_MEMSET_S)
  memset_s(buffer, size, value, size);
#elif defined(LIBERATE_HAVE_MEMSET_EXPLICIT)
  memset_explicit(buffer, value, size);
#elif defined(LIBERATE_HAVE_EXPLICIT_MEMSET)
  explicit_memset(buffer, value, size);
#else
  // Fall back to an implementation using volatile to ensure it's not optimized
  // away.
  volatile char * buf = static_cast<char *>(buffer);
  size_t to_remove = size;
  while (to_remove--) {
    *buf++ = value;
  }
#endif
}


LIBERATE_API void
secure_memzero(void * buffer, size_t size)
{
#if defined(LIBERATE_HAVE_EXPLICIT_MEMZERO)
  explicit_memzero(buffer, size);
#elif defined(LIBERATE_HAVE_MEMZERO_EXPLICIT)
  memzero_explicit(buffer, size);
#elif defined(LIBERATE_HAVE_EXPLICIT_BZERO)
  explicit_bzero(buffer, size);
#elif defined(LIBERATE_HAVE_SECURE_ZERO_MEMORY)
  SecureZeroMemory(buffer, size);
#else
  // Fall back to secure memset
  secure_memset(buffer, size, 0);
#endif
}


} // namespace liberate::sys
