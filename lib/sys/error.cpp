/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <liberate/sys/error.h>

#include <sstream>
#include <ios>

#if defined(LIBERATE_POSIX)
#  include <string.h>
#else
#  include "../net/netincludes.h"
#  include <liberate/string/utf8.h>
#endif

namespace liberate::sys {

namespace {

#if defined(LIBERATE_POSIX)

static constexpr char const * const FALLBACK_ERROR = "Error copying error "
  "message.";

std::string
strerror_helper(int code)
{
  static char buf[1024] = { 0 }; // flawfinder: ignore
#if defined(LIBERATE_HAVE_STRERROR_S)
  auto err = ::strerror_s(buf, sizeof(buf), code);
  if (err) {
    return FALLBACK_ERROR;
  }
  return buf;
#elif defined(LIBERATE_HAVE_STRERROR_R)
#  if (defined(_POSIX_C_SOURCE) && (_POSIX_C_SOURCE < 200112L)) || _GNU_SOURCE
  // GNU-specific
  char * err = ::strerror_r(code, buf, sizeof(buf));
  if (!err) {
    return FALLBACK_ERROR;
  }
  return err;
#  else
  // XSI compliant
  int err = ::strerror_r(code, buf, sizeof(buf));
  if (err) {
    return FALLBACK_ERROR;
  }
  return buf;
#  endif
#else
  // Fall back to plain strerror
  return ::strerror(code);
#endif
}

#endif

} // anonymous namespace



int error_code()
{
#if defined(LIBERATE_POSIX)
  return errno;
#else
  return WSAGetLastError();
#endif
}



std::string
error_message(int code)
{
#if defined(LIBERATE_POSIX)
  std::stringstream msg;
  msg << "[0x" << std::hex << code << std::dec << " (" << code << ")] "
    << strerror_helper(code);
  return msg.str();
#else
  TCHAR * errmsg = NULL;
  FormatMessageW(
      FORMAT_MESSAGE_ALLOCATE_BUFFER
        |FORMAT_MESSAGE_FROM_SYSTEM
        |FORMAT_MESSAGE_IGNORE_INSERTS,
      NULL, code,
      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
      (LPWSTR) &errmsg, 0, NULL);

  std::stringstream msg;
  msg << "[0x" << std::hex << code << std::dec << " (" << code << ")] "
      << ::liberate::string::to_utf8(errmsg);

  LocalFree(errmsg);
  return msg.str();
#endif
}


} // namespace liberate::sys
