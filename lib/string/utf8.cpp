/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/string/utf8.h>

#if defined(LIBERATE_WIN32)

#include <vector>

namespace liberate::string {

std::string
to_utf8(TCHAR const * source)
{
  size_t size = WideCharToMultiByte(CP_UTF8, 0, source, -1, nullptr, 0,
      nullptr, nullptr);
  std::vector<char> buf(size, '\0');
  WideCharToMultiByte(CP_UTF8, 0, source, -1, &buf[0], size, nullptr, nullptr);
  return {buf.begin(), buf.end() - 1};
}



std::wstring
from_utf8(char const * source)
{
  size_t size = MultiByteToWideChar(CP_UTF8, 0, source, -1, nullptr, 0); // flawfinder: ignore
  std::vector<wchar_t> buf(size, 0);
  MultiByteToWideChar(CP_UTF8, 0, source, -1, &buf[0], size); // flawfinder: ignore
  return {buf.begin(), buf.end() - 1};
}


} // namespace liberate::string

#endif // Win32
