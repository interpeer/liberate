/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/string/util.h>

#include <algorithm>
#include <locale>
#include <cctype>
#include <sstream>
#include <iomanip>

namespace liberate::string {

std::string
to_lower(std::string const & input)
{
  std::string ret;
  ret.resize(input.size());
  std::transform(
      input.begin(), input.end(), ret.begin(),
      [] (std::string::value_type ch) -> std::string::value_type
      {
        return std::tolower<std::string::value_type>(
            ch, std::locale());
      }
  );
  return ret;
}


std::string
to_upper(std::string const & input)
{
  std::string ret;
  ret.resize(input.size());
  std::transform(
      input.begin(), input.end(), ret.begin(),
      [] (std::string::value_type ch) -> std::string::value_type
      {
        return std::toupper<std::string::value_type>(
            ch, std::locale());
      }
  );
  return ret;
}



std::string
replace(std::string const & haystack, std::string const & needle,
    std::string const & substitute, bool first_only /* = false */)
{
  std::string ret;

  std::string::size_type start = 0;
  do {
    auto delim_pos = haystack.find(needle, start);

    // Skip until the end.
    if (delim_pos == std::string::npos) {
      ret += haystack.substr(start);
      break;
    }

    // Take everything up until the needle, then add substitute
    ret += haystack.substr(start, delim_pos - start);
    ret += substitute;

    // Advance start by length of needle.
    start = delim_pos + needle.size();

    // If we only wanted the first match, bail out early.
    if (first_only) {
      ret += haystack.substr(start);
      break;
    }
  } while (start < haystack.length());

  return ret;
}


namespace {

//
// Templates case-insensitive equal
//
template <typename T>
struct case_insensitive_equal
{
  std::locale const & m_loc;

  inline explicit case_insensitive_equal(std::locale const & loc)
    : m_loc(loc)
  {
  }

  inline bool operator()(T const & first, T const & second)
  {
    return std::tolower(first, m_loc) == std::tolower(second, m_loc);
  }
};


} // anonymous namespace



ssize_t
ifind(std::string const & haystack, std::string const & needle)
{
  auto it = std::search(haystack.begin(), haystack.end(),
      needle.begin(), needle.end(),
      case_insensitive_equal<std::string::value_type>(std::locale()));
  if (it == haystack.end()) {
    return -1;
  }
  return it - haystack.begin();
}


namespace {

template <typename delimT>
inline std::string
get_word(std::istream & ss, delimT d)
{
  std::string word;
  for (char ch ; ss.get(ch) ; ) {
    if (!d(ch)) {
      word.push_back(ch);
      break;
    }
  }
  for (char ch ; ss.get(ch); ) {
    if (!d(ch)) {
      word.push_back(ch);
    }
    else {
      break;
    }
  }
  return word;
}

} // anonymous namespace


std::vector<std::string>
split(std::string const & haystack, std::string const & delimiters)
{
  std::stringstream ss(haystack);
  auto del = [&](char ch) {
    for (auto x : delimiters) {
      if (x == ch) return true;
    }
    return false;
  };

  std::vector<std::string> words;
  for (std::string w ; (w = get_word(ss, del)) != ""; ) {
    words.push_back(w);
  }
  return words;
}

} // namespace liberate::string
