/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/string/urlencode.h>

#include <liberate/string/hexencode.h>

namespace liberate::string {

std::string
urlencode(std::string const & input)
{
  std::string ret;

  for (auto ch : input) {
    // Keep alphanumeric and other accepted characters intact
    if (isalnum(ch) || ch == '-' || ch == '_' || ch == '.' || ch == '/') {
      ret += ch;
      continue;
    }

    ret += "%";
    ret += hexencode(&ch, 1, true);
  }

  return ret;
}



std::string
urldecode(std::string const & input)
{
  std::string ret;

  for (std::string::size_type i = 0 ; i < input.size() ; ++i) {
    auto ch = input[i];

    // Percent-encoded
    if (ch == '%') {
      ::liberate::types::byte val;
      auto used = hexdecode(&val, 1,
          reinterpret_cast<::liberate::types::byte const *>(
            input.c_str() + i + 1
          ), 2);
      if (used > 0) {
        ret += static_cast<char>(val);
      }
      else {
        ret += "?";
      }
      i += 2;
      continue;
    }

    // Regular value
    ret += ch;
  }

  return ret;
}

} // namespace liberate::string
