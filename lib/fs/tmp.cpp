/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <liberate/fs/tmp.h>

#include <stdexcept>

#if defined(LIBERATE_WIN32)

#include <chrono>
#include <sstream>

#include <liberate/string/utf8.h>

namespace liberate::fs {

std::string
temp_name(std::string const & prefix /* = "" */)
{
  // Temporary directory
  wchar_t tmpdir[MAX_PATH + 1] = { 0 }; // flawfinder: ignore
  auto len = GetTempPath(sizeof(tmpdir) / sizeof(wchar_t), tmpdir);
  if (len <= 0) {
    throw std::runtime_error{"GetTempPath() failed."};
  }

  // Now get the temporary file name
  auto now = std::chrono::steady_clock::now();
  std::stringstream ts;
  ts << std::hex << now.time_since_epoch().count();

  // Generate return value
  std::string ret = string::to_utf8(tmpdir);
  if (prefix.empty()) {
    ret += "liberate";
  }
  else {
    ret += prefix;
  }
  ret += "-" + ts.str() + ".tmp";

  return ret;
}

} // namespace liberate::fs

#else // LIBERATE_WIN32

#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <vector>

#include <liberate/string/hexencode.h>
#include <liberate/random/unsafe_bits.h>

#if defined(PATH_MAX)
#  define LIBERATE_TEMP_MAX PATH_MAX
#elif defined(UNIX_PATH_MAX)
#  define LIBERATE_TEMP_MAX UNIX_PATH_MAX
#else
#  define LIBERATE_TEMP_MAX 1024
#endif

#define LIBERATE_DEFAULT_TMP "/tmp"

namespace liberate::fs {

std::string
temp_name(std::string const & prefix /* = "" */)
{
  // Get temporary directory
  char const * tmpdir = getenv("TMPDIR"); // flawfinder: ignore
  if (!tmpdir) {
    tmpdir = LIBERATE_DEFAULT_TMP;
  }
  else {
    size_t safe_len = strnlen(tmpdir, LIBERATE_TEMP_MAX);
    if (safe_len == LIBERATE_TEMP_MAX) {
      tmpdir = LIBERATE_DEFAULT_TMP;
    }
  }

  // Create some random number and hex encode it. This is not *very* safe,
  // but it improves upon plain mkstemp() according to
  // https://cwe.mitre.org/data/definitions/377.html
  liberate::random::unsafe_bits<uint64_t> gen; // flawfinder: ignore
  auto bits = gen.get();
  auto rand = liberate::string::hexencode(
      static_cast<char const *>(static_cast<void const *>(&bits)), sizeof(bits));

  // Template
  std::string templ = tmpdir;
  if (prefix.empty()) {
    templ += "/liberate-" + rand + "-XXXXXX";
  }
  else {
    templ += "/" + prefix + "-" + rand + "-XXXXXX";
  }

  // Copy to writable buffer
  std::vector<char> buf{templ.begin(), templ.end()};
  buf.push_back(0);

  // Create temporary file
  int fd = mkstemp(&buf[0]); // flawfinder: ignore
  if (fd < 0) {
    throw std::runtime_error{"mkstemp() failed."};
  }
  std::string ret{buf.begin(), buf.end()};

  // Cleanup
  close(fd);
  unlink(ret.c_str());

  return ret;
}


} // namespace liberate::util

#endif // LIBERATE_WIN32
