/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <liberate/net/socket_address.h>

#include <cstring>
#include <sstream>
#include <algorithm>

#include <liberate/fs/path.h>

#include <liberate/logging.h>

#include "cidr.h"

#include "../macros.h"
#include "cidr.h"
#include "socket_address_impl.h"


namespace liberate::net {

/*****************************************************************************
 * Helper functions
 **/
namespace {


template <typename T>
char const * unwrap(T const & source)
{
  // cppcheck-suppress CastIntegerToAddressAtReturn
  return source;
}

template<>
char const * unwrap<std::string>(std::string const & source)
{
  return source.c_str();
}


template <typename T>
void
parse_address(detail::address_data & data, T const & source, size_t size,
    uint16_t port)
{
  // Need to zero data.
  sys::secure_memzero(&data.sa_storage, sizeof(data));

  // Try parsing as a CIDR address.
  detail::parse_result_t result{data};
  try {
    detail::parse_extended_cidr(source, true, result, port);
    return;
  } catch (std::invalid_argument const & ex) {
    // Try with the below code.
    LIBLOG_DEBUG("Unable to parse extended CIDR, trying as path name.");
  }

#if defined(LIBERATE_HAVE_SOCKADDR_UN)
  // If parsing got aborted, we either have AF_UNIX or AF_UNSPEC as the
  // actual socket type.
#if defined(LIBERATE_WIN32)
  std::string converted = fs::to_win32_path(source);
  auto unwrapped = converted.c_str();
#else // LIBERATE_WIN32
  auto unwrapped = unwrap(source);
#endif // LIBERATE_WIN32
  sys::secure_memzero(&data.sa_storage, sizeof(data));
  data.sa_un.sun_family = size > 0 ? AF_UNIX : AF_UNSPEC;
  ::memcpy(data.sa_un.sun_path, unwrapped, // flawfinder: ignore
      std::min(size, size_t{UNIX_PATH_MAX}));
#endif
}




} // anonymous namespace




/*****************************************************************************
 * Member functions
 **/
socket_address::socket_address()
  : m_impl{std::make_shared<socket_address_impl>()}
{
}



socket_address::socket_address(void const * buf, size_t len)
  : m_impl{std::make_shared<socket_address_impl>(buf, len)}
{
}



socket_address::socket_address(address_type type, void const * buf, size_t len,
    uint16_t port /* = 0 */)
  : m_impl{std::make_shared<socket_address_impl>(type, buf, len, port)}
{
}



socket_address::socket_address(std::string const & address,
    uint16_t port /* = 0 */)
  : socket_address{}
{
  parse_address(m_impl->data, address, address.size(), port);
}



socket_address::socket_address(char const * address, uint16_t port /* = 0 */,
    size_t size /* = 0 */)
  : socket_address{}
{
  // We're ignoring this flawfinder finding below because the function is
  // deliberately flexible on whether the size is given. Of course we'd prefer
  // a size.
  parse_address(m_impl->data, address,
      address && size > 0 ? size : ::strlen(address), // flawfinder: ignore
      port);
}



socket_address::socket_address(socket_address const & other)
  : m_impl{std::make_shared<socket_address_impl>(*other.m_impl)}
{
}



socket_address &
socket_address::operator=(socket_address const & other)
{
  m_impl = std::make_shared<socket_address_impl>(*other.m_impl);
  return *this;
}



bool
socket_address::verify_cidr(std::string const & address)
{
  detail::address_data dummy_addr;
  detail::parse_result_t result{dummy_addr};
  try {
    return detail::parse_extended_cidr(address, true, result);
  } catch (std::invalid_argument const &) {
    return false;
  }
}



size_t
socket_address::max_netmask() const
{
  return m_impl->max_netmask();
}




bool
socket_address::verify_netmask(size_t const & netmask) const
{
  return netmask <= max_netmask();
}



std::string
socket_address::cidr_str() const
{
  return m_impl->cidr_str();
}



uint16_t
socket_address::port() const
{
  return m_impl->port();
}



std::string
socket_address::full_str() const
{
  return m_impl->full_str();
}



size_t
socket_address::bufsize() const
{
  return m_impl->bufsize();
}



void const *
socket_address::buffer() const
{
  return m_impl->buffer();
}



void *
socket_address::buffer()
{
  return m_impl->buffer();
}



size_t
socket_address::min_bufsize(bool with_type, bool with_port) const
{
  return calculate_minsize(type(), with_type, with_port);
}



size_t
socket_address::serialize(void * buf, size_t len, bool with_type,
    bool with_port) const
  OCLINT_SUPPRESS("high cyclomatic complexity")
  OCLINT_SUPPRESS("long method")
{
  return m_impl->serialize(buf, len, with_type, with_port);
}



std::tuple<size_t, socket_address>
socket_address::deserialize(address_type type, void const * buf, size_t len,
    bool with_port)
  OCLINT_SUPPRESS("high cyclomatic complexity")
{
  if (type != AT_INET4 && type != AT_INET6) {
    return std::make_tuple(0, socket_address{});
  }

  if (!len) {
    return std::make_tuple(0, socket_address{});
  }

  auto remaining = calculate_minsize(type, false, with_port);
  if (remaining > len) {
    return std::make_tuple(0, socket_address{});
  }

  auto offset = reinterpret_cast<uint8_t const *>(buf);

  socket_address tmp;
  switch (type) {
    case AT_INET4:
      {
        tmp.m_impl->data.sa_storage.ss_family = AF_INET;

        auto res = sizeof(in_addr);
        if (remaining < res) {
          return std::make_tuple(0, socket_address{});
        }
        memcpy(&(tmp.m_impl->data.sa_in.sin_addr), offset, res); // flawfinder: ignore

        offset += res;
        remaining -= res;
      }
      break;

    case AT_INET6:
      {
        tmp.m_impl->data.sa_storage.ss_family = AF_INET6;

        auto res = sizeof(in6_addr);
        if (remaining < res) {
          return std::make_tuple(0, socket_address{});
        }

        memcpy(&(tmp.m_impl->data.sa_in6.sin6_addr), offset, res); // flawfinder: ignore

        offset += res;
        remaining -= res;
      }
      break;

    default:
      break;
  }

  // Deserialize port, if necessary
  if (with_port) { //!OCLINT
    auto portp = tmp.m_impl->data.sa_storage.ss_family == AF_INET
      ? &tmp.m_impl->data.sa_in.sin_port
      : &tmp.m_impl->data.sa_in6.sin6_port;

    auto required = sizeof(*portp);
    if (remaining < required) {
      return std::make_tuple(0, socket_address{});
    }

    memcpy(portp, offset, required); // flawfinder: ignore

    offset += required;
    // cppcheck-suppress unreadVariable
    remaining -= required;
  }

  return std::make_tuple(
      offset - reinterpret_cast<uint8_t const *>(buf),
      tmp);
}



std::tuple<size_t, socket_address>
socket_address::deserialize(void const * buf, size_t len, bool with_port)
{
  if (!len) {
    return std::make_tuple(0, socket_address{});
  }

  // Deserialize the type.
  auto ptr = reinterpret_cast<uint8_t const *>(buf);
  address_type type = static_cast<address_type>(*ptr);

  // Pass the rest of the buffer on to the typed function.
  auto [size, addr] = deserialize(type, ptr + 1, len - 1, with_port);
  if (!size) {
    return std::make_tuple(0, socket_address{});
  }
  return std::make_tuple(size + 1, addr);
}



bool
socket_address::set_port(uint16_t port)
{
  return m_impl->set_port(port);
}



bool
socket_address::is_any() const
{
  return m_impl->is_any();
}



bool
socket_address::is_loopback() const
{
  return m_impl->is_loopback();
}



bool
socket_address::is_equal_to(socket_address const & other) const
{
  return m_impl->is_equal_to(*other.m_impl);
}



bool
socket_address::is_less_than(socket_address const & other) const
{
  return m_impl->is_less_than(*other.m_impl);
}



void
socket_address::operator++()
{
  m_impl->increment();
}



address_type
socket_address::type() const
{
  return m_impl->type();
}



size_t
socket_address::hash() const
{
  return m_impl->hash();
}



void socket_address::swap(socket_address & other)
{
  return m_impl->swap(*other.m_impl);
}



/*****************************************************************************
 * Friend functions
 **/
std::ostream &
operator<<(std::ostream & os, socket_address const & addr)
{
  os << addr.full_str();
  return os;
}


} // namespace liberate::net
