/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef LIBERATE_NETINCLUDES_H
#define LIBERATE_NETINCLUDES_H

#include <build-config.h>

#if defined(LIBERATE_HAVE_ARPA_INET_H)
#include <arpa/inet.h>
#endif

#if defined(LIBERATE_HAVE_NETINET_IN_H)
#include <netinet/in.h>
#endif

#if defined(LIBERATE_HAVE_LINUX_UN_H)
#  include <linux/un.h>
#  define LIBERATE_HAVE_SOCKADDR_UN
#else
#  if defined(LIBERATE_HAVE_SYS_UN_H)
#    include <sys/un.h>
#    define UNIX_PATH_MAX 108
#    define LIBERATE_HAVE_SOCKADDR_UN
#  endif
#endif

#if defined(LIBERATE_HAVE_SYS_SOCKET_H)
#  include <sys/socket.h>
#endif

#if defined(LIBERATE_HAVE_WINSOCK2_H)
#  define FD_SETSIZE 32767
#  include <winsock2.h>
#  pragma comment(lib, "Ws2_32.lib")
#endif

#if defined(LIBERATE_HAVE_WS2TCPIP_H)
#  include <ws2tcpip.h>
#endif

#if defined(LIBERATE_HAVE_AFUNIX_H)
#  include <afunix.h>
#  define LIBERATE_HAVE_SOCKADDR_UN
#endif

#if defined(LIBERATE_WIN32)
using sa_family_t = ADDRESS_FAMILY;
#endif

#endif // guard
