/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef LIBERATE_NET_CIDR_H
#define LIBERATE_NET_CIDR_H

// *** Config
#include <liberate.h>
#include <build-config.h>

#include <liberate/net/socket_address.h>

#include "netincludes.h"

// *** C++ includes
#include <string>

namespace liberate::net::detail {


/**
 * Union for avoiding casts and breaking aliasing rules. We know by definition
 * that sockaddr_storage is the largest of these.
 **/
union address_data
{
  sockaddr          sa;
  sockaddr_in       sa_in;
  sockaddr_in6      sa_in6;
  sockaddr_storage  sa_storage;
#if defined(LIBERATE_HAVE_SOCKADDR_UN)
  sockaddr_un       sa_un;
#endif
  // Pad to a minimum size; unused union member.
  ::liberate::types::byte _dummy[socket_address::bufsize_available()];
};


/**
 * Parses a CIDR-notation network specification into a socketaddr_storage
 * sized buffer (setting the port part to 0), and a bitmask length.
 *
 * The results are stored in parse_result_t. It will contain the detected
 * protocol type, parsed address, and mask size (if applicable).
 *
 * The return value is an error_t, with ERR_SUCCESS indicating absolute
 * parse success.
 *
 * ERR_INVALID_VALUE is returned if something about the address specification
 * does not meet the requirements, e.g. a CIDR address with a port *and*
 * netmask specification (with port is valid, with netmask is valid, but not
 * both).
 *
 * ERR_ABORTED is returned if no CIDR format could be detected. It's possible
 * that the address is of non-CIDR type, such as a local path.
 *
 * If the no_mask flag is set, this function expects *no* netmask part to
 * the string, and can be used to parse IPv4 and IPv6 host addresses.
 *
 * The CIDR specification is extended in that we also parse ports, if specified.
 * Note that any argument to the port parameter will override the port
 * specification found in the cidr string.
 *
 * For IPv4 and IPv6, the port is specified after the address part, separated by
 * a colon. For IPv6, the address part additionally needs to be enclosed in
 * square brackets. Note that if a port is specified, a netmask cannot be and
 * vice versa.
 **/

struct LIBERATE_PRIVATE parse_result_t
{
  inline explicit parse_result_t(address_data & data)
    : proto(AF_UNSPEC)
    , address(data)
    , mask(-1)
  {
  }

  sa_family_t     proto;    // Protocol detection.
  address_data &  address;  // The address.
  ssize_t         mask;     // For AF_INET*
};


LIBERATE_PRIVATE
bool
parse_extended_cidr(std::string const & cidr, bool no_mask,
    parse_result_t & result, uint16_t port = 0);

} // namespace liberate::net::detail

#endif // guard
