---
on:
  push:
    branches:
      - main
      - 'release/**'
    tags:
      - 'v*.*.*'
  pull_request:
    branches:
      - main
      - 'release/**'

jobs:
  appveyor:
    runs-on: [docker, x86_64]
    steps:
      - name: Trigger AppVeyor
        uses: https://codeberg.org/Native-CI/action-appveyor@v2
        with:
          account: ${{ secrets.APPVEYOR_ACCOUNT }}
          token: ${{ secrets.APPVEYOR_TOKEN }}
          slug: vessel

  checks:
    runs-on: [docker, x86_64]
    if: github.event_name == 'pull_request'
    steps:
      - name: Clone
        uses: actions/checkout@v4.1.1
        with:
          submodules: true
          fetch-depth: 0

      - name: Towncrier check
        uses: https://codeberg.org/Native-CI/action-towncrier@v1.1

      - name: Static analysis
        uses: https://codeberg.org/Native-CI/action-static-analysis@v1

  build-meson:
    runs-on: [docker, "${{ matrix.arch }}"]
    strategy:
      matrix:
        arch: [x86_64, arm64]
        compilers:
          - cc: "gcc"
            cxx: "g++"
          - cc: "clang"
            cxx: "clang++"
        include:
          - arch: x86_64
            compilers:
              cc: "gcc"
              cxx: "g++"
            coverage: true
            sanitize: true
    steps:
      - name: Clone
        uses: actions/checkout@v4.1.1
        with:
          submodules: true

      - name: Build
        uses: https://codeberg.org/Native-CI/action-meson@v2.1
        with:
          cc: ${{ matrix.compilers.cc }}
          cxx: ${{ matrix.compilers.cxx }}
          coverage: ${{ matrix.coverage }}
          sanitize: ${{ matrix.sanitize }}

  build-conan:
    runs-on: [docker, "${{ matrix.arch }}"]
    strategy:
      matrix:
        arch: [x86_64, arm64]
    steps:
      - name: Clone
        uses: actions/checkout@v4.1.1
        with:
          submodules: true

      - name: Build
        uses: https://codeberg.org/Native-CI/action-conan@v1
        with:
          conan_push_url: https://codeberg.org/api/packages/interpeer/conan
          conan_push_user: action
          conan_push_password: ${{ secrets.CONAN_PUSH_TOKEN }}

  build-android:
    runs-on: [docker, x86_64]
    strategy:
      matrix:
        profile:
          - android-arm64-v8a.conan
          - android-armeabi-v7a.conan
          - android-x86.conan
          - android-x86_64.conan
    steps:
      - name: Clone
        uses: actions/checkout@v4.1.1
        with:
          submodules: true

      - name: Build
        uses: https://codeberg.org/Native-CI/action-conan-android@v1
        with:
          profile: ${{ matrix.profile }}
          conan_push_url: https://codeberg.org/api/packages/interpeer/conan
          conan_push_user: action
          conan_push_password: ${{ secrets.CONAN_PUSH_TOKEN }}
