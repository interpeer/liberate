====================================
Welcome to liberate's documentation!
====================================

This library has two purposes: to provide some cross-platform abstractions,
and to provide often used utility functionality for other `interpeer projects`_.

.. _interpeer projects: https://interpeer.io/projects/

Using this Library
==================

:doc:`getting-started`
    Getting started with the library.

:doc:`howtos/index`
    How-To Guides for using parts of this library.

:doc:`api/lib`
    Generated API documentation.


.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   self
   getting-started
   howtos/index
   api/lib

.. todolist::
