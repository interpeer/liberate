# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'liberate'
copyright = '2023, Interpeer gUG'
author = 'Interpeer gUG'
rst_prolog = '''
.. |license| replace:: CC-BY-SA
.. _license: https://creativecommons.org/licenses/by-sa/4.0/
'''
version = 'v0.3.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'breathe',
    'exhale',
    'sphinx_sitemap',
]

templates_path = ['_templates']
exclude_patterns = []

intersphinx_mapping = {
    # Nothing to see here
}

todo_include_todos = True

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

html_theme_options = {
    'display_version': True,
    'style_external_links': True,
    'style_nav_header_background': '#0a4d50', # forest
}

html_baseurl = 'https://docs.interpeer.io/liberate/'

# XXX The file does *not* exist in this directory, but it will exist when
#     the site is generated from https://codeberg.org/interpeer/docs/
html_css_files = (
    'css/docs.css',
)


# -- Breathe configuration ---------------------------------------------------
breathe_projects = {
    "liberate": "./xml/",
}

breathe_projects_source = {
    "liberate" : ( "../../../include", ()),
}

breathe_default_project = "liberate"

breathe_default_members = ('members', 'undoc-members')

# -- Exhale configuration ---------------------------------------------------

predefined = {
    'LIBERATE_API': '',
    'LIBERATE_POSIX': '',
    'LIBERATE_WIN32': '',
}
predefined = [f'"{k}={v}"' for k, v in predefined.items()]

defined_as_is = (
    'LIBERATE_START_ERRORS',
    'LIBERATE_END_ERRORS',
    'LIBERATE_ERRDEF',
)

exclude = (
    'LIBERATE_POSIX', 'LIBERATE_WIN32',
    'LIBERATE_PLATFORM_DEFINED',
    'WIN32_LEAN_AND_MEAN',
    '__UNDEF_LEAN_AND_MEAN',
    'NOMINMAX',
    'UNICODE', '_UNICODE',
    'LIBERATE_API', 'LIBERATE_API_FRIEND', 'LIBERATE_PRIVATE',
    'LIBERATE_ERROR_FUNCTIONS',
    'detail::*', '*::detail::*',
) + defined_as_is

import os.path
prefix = __file__.split(os.path.sep)[:-4]
prefix.append('include')
prefix = os.path.sep.join(prefix)

breathe_doxygen_config_options = {
    'INPUT': prefix,
    'WARN_IF_UNDOCUMENTED': 'YES',
    'WARN_IF_INCOMPLETE_DOC': 'YES',
    'EXTRACT_ANON_NSPACES': 'NO',

    'EXPAND_AS_DEFINED': ' '.join(defined_as_is),
    'EXCLUDE_SYMBOLS': ' '.join(exclude),

    'OPTIMIZE_OUTPUT_FOR_C': 'NO',
}

doxy_stdin = '\n'.join([f'{k} = {v}' for k, v in breathe_doxygen_config_options.items()])
doxy_stdin += '\nPREDEFINED += ' + ' '.join(predefined)

import textwrap, exhale.configs

# The configurations you specified
external_configs = textwrap.dedent(doxy_stdin)

# The full input being sent
full_input = "{base}\n{external}\n\n".format(
    base=exhale.configs.DEFAULT_DOXYGEN_STDIN_BASE,
    external=external_configs
)

# Setup the exhale extension
exhale_args = {
    # These arguments are required
    "containmentFolder":     "./api",
    "rootFileName":          "lib.rst",
    "doxygenStripFromPath":  prefix,
    # Heavily encouraged optional argument (see docs)
    "rootFileTitle":         "API Documentation",
    # Suggested optional arguments
    "createTreeView":        True,
    # TIP: if using the sphinx-bootstrap-theme, you need
    # "treeViewIsBootstrap": True,
    "exhaleExecutesDoxygen": True,
    "exhaleDoxygenStdin":    full_input,
}

# Tell sphinx what the primary language being documented is.
primary_domain = 'cpp'

# Tell sphinx what the pygments highlight language should be.
highlight_language = 'cpp'

