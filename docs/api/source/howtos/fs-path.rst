=======================================================
How-To convert filesystem paths between WIN32 and POSIX
=======================================================

This library functionality helps when ``std::filesystem::path`` is unavailable
or too heavy-weight. Often, one just needs to abstract out file system path
differences between POSIX-like systems and Windows.

We provide two functions for converting back and forth between such paths, largely
replacing `\\` with `/` and vice versa.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Convert between the formats.

    #include <liberate/fs/path.h>

    std::string posix_style{"/foo/bar"};

    auto win32_style = liberate::fs::to_win32_path(posix_style);
    auto back = liberate::fs::to_posix_path(win32_style);

    assert(posix_style == back);

.. note::
  The :cpp:func:`liberate::fs::to_posix_path` function is similar to the
  `generic_string()`_ function of ``std::filesystem::path``, but works on
  plain ``std::string``.

.. _generic_string(): https://en.cppreference.com/w/cpp/filesystem/path/generic_string
