=============================
How-To work with URL encoding
=============================

When working with the :cpp:class:`liberate::net::url` class, it may be useful
to encode and decode strings using the `percent encoding`_ defined in RFC3986.
For the most part, this amounts to replacing some reserved characters with
a percent sign followed by the hexadecimal value of the ASCII code representing
the character -- so that ` ` becomes `%20`, etc.

.. _percent encoding: https://datatracker.ietf.org/doc/html/rfc3986#section-2.1

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Using URL encoding

    #include <liberate/string/urlencode.h>

    auto res = liberate::string::urlencode("foo bar");
    assert ("foo%20bar" == res);

Note that the ``urlencode`` function only encodes some reserved characters,
while the ``urldecode`` function decodes any percent-encoded character. This
means they do not provide the exact inverse function of each other.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Using URL encoding

    auto res = liberate::string::urldecode("foo%2Fbar");
    assert("foo/bar" == res);

    auto back = liberate::string::urlencode(res);
    assert("foo/bar" == back);
    assert(res != back);

.. seealso::
   - :doc:`net-url`
