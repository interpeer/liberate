====================================
How-To generate temporary file names
====================================

POSIX and WIN32 both provide APIs for retrieving the name of a directory in
which temporary files may be stored. Of course, both APIs work very
differently from each other. Additionally, actually `creating temporary file can be vulnerable`_
-- in particular, security guarantees one could make are not portable.

.. warning::
   The functions in this section are **not** secure, but they *do* provide
   reasonably unique file names. If you need a secure function, please consider
   a different approach.

The main point of this function is to provide a file name that is in the
system's temporary directory, and should not typically collide with other
attempts to create such files.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Create temporary names

    #include <liberate/fs/tmp.h>

    auto tmp = liberate::fs::temp_name("my-prefix");

The prefix argument is optional and may be omitted.

.. _creating temporary file can be vulnerable: https://cwe.mitre.org/data/definitions/377.html
