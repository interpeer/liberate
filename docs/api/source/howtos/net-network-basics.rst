=======================
How-To create a network
=======================

The :cpp:class:`liberate::net::network` class exists for making working with
IPv4 and IPv6 networks easier. In particular, this means the class makes it
easy to access special addresess within it.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Create a network from CIDR-style notation

    #include <liberate/net/network.h>

    liberate::net::network net{"192.168.0.3/16"};

You can, of course, access the "local" address easily enough, which is just
the address you passed to the constructor. But you can also get the network
part of the address only, the gateway and broadcast addresses.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Access the local address

    net.local_address()     // == 192.168.0.3
    net.network_address()   // == 192.168.0.0
    net.gateway_address()   // == 192.168.0.1
    net.broadcast_address() // == 192.168.255.255

.. note::
   There is no need for a gateway to be configured at ``gateway_address()``,
   but that is common practice.
