=========================================
How-To share data with a concurrent queue
=========================================

Working with threads often means distributing stuff from one thread to the next.
And doing that also means taking into account that one thread might be slower
than the other. Arguably, one of the main use cases of threads is to keep a user
interface thread responsive, whilst working on some work packages in the
background.

For this, this library includes :cpp::class:`liberate::concurrency::concurrent_queue`.

This is not entirely lock free -- it uses user-space spin locks -- but it does
not require system primitives for serializing access to data and so is pretty
much free of overhead *unless* there is contention.

The queue has an interface reminiscent of standard library containers, but it's
not quite the same. Data used in the queue is expected to be `CopyConstructable`.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Concurrent queue; push work

    #include <liberate/concurrency/concurrent_queue.h>

    using namespace liberate::concurrency;

    concurrent_queue<my_data_type> queue;

    // Push work into the queue
    my_data_type data{/* ... */};
    queue.push(data);

Pushing work in the form of data is simple, as you can see. You can also
push a range of values from a container.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Concurrent queue; push more work

    std::vector<my_data_type> more_data;
    queue.push_range(more_data.begin(), more_data.end());

Retrieving data is a little different from how you might expect; it requires
returning two values: a boolean indicating whether work was available, and
if so, the value itself.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Concurrent queue; retrieve work

    auto [success, data] = queue.pop();
    if (success) {
      // use data
    }

.. seealso::
   :doc:`concurrency-command`
