==============================
How-To deal with system errors
==============================

One of the simpler parts of writing platform independent code that nonetheless
annoys easily is that different platforms define different error codes, and
have different APIs for accessing them -- and for accessing error messages
matching the codes.

This library provides an abstraction for this.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Platform errors

    #include <liberate/sys/errors.h>

    auto code = liberate::sys::error_code();
    auto msg = liberate::sys::error_message(code);

- :cpp:func:`liberate::sys::error_code` invokes ``WSAGetLastError()`` or
  returns ``errno``.
- :cpp:func:`liberate::sys::error_message` invokes whatever platform
  function there is for retrieving an associated error message, and transcodes
  the result to UTF-8 before returning.
