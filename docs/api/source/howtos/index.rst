=============
How-To Guides
=============

This library has two purposes: to provide some cross-platform abstractions,
and to provide often used utility functionality for other `interpeer projects`_.

For this reason, it seems prudent to cut to the chase, and provide guides for
how to accomplish certain tasks with this library. These are sorted by
related concerns, which are also reflected in the include directories and
namespaces of the library.

.. _interpeer projects: https://interpeer.io/projects/

Support with C++ Types
======================

:doc:`cpp-comparison`
    Making complex C++ types comparable, e.g. for use in sorted containers
    such as ``std::map``.

:doc:`cpp-hashing`
    Create hashes from C++ types, and make them usable in e.g. unsorted
    containers such as ``std::unordered_map``.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   cpp-comparison
   cpp-hashing

Special C++ Types
=================

:doc:`types-byte`
    Using bytes -- and avoiding both ``char *`` and ``void *`` in the process.

:doc:`types-varint`
    Use a special integer type to signal that you might want to serialize it
    in variable length encoding.

:doc:`types-traits`
    Using extended type traits.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   types-byte
   types-varint
   types-traits

Serialization
=============

:doc:`serialization-integer`
    Serialize and deserialize integer types.

:doc:`serialization-varint`
    Variable sized integer serialization and deserialization.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   serialization-integer
   serialization-varint

Filesystem Utilities
====================

:doc:`fs-path`
    Convert between POSIX-style and WIN32-style paths.

:doc:`fs-tmp`
    Generate names for temporary files in appropriate scratch directories.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   fs-path
   fs-tmp

Socket and Network Utilities
============================

:doc:`net-socket-address-platform`
    Use different types of socket address and interface with system APIs
    for this.

:doc:`net-socket-address-parse`
    Create socket addresses from CIDR-style strings.

:doc:`net-socket-address-scope`
    Work with network masks and similar scope issues.

:doc:`net-network-basics`
    Create a network, and determine some special addresses in it.

:doc:`net-network-management`
    Manage address assignments in a network.

:doc:`net-url`
    Parse and manipulate Uniform Resource Locators (URLs).

:doc:`net-ip`
    Parse addresses from IPv4 and IPv6 packet headers.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   net-socket-address-platform
   net-socket-address-parse
   net-socket-address-scope
   net-network-basics
   net-network-management
   net-url
   net-ip

String Utilities
================

:doc:`string-utf8`
    Convert to- and from UTF-8 strings on Windows.

:doc:`string-util`
    Perform some common string manipulations, such as upper- and lowercasing
    strings, finding parts, or splitting them.

:doc:`string-urlencode`
    Use URL encoding for strings.

:doc:`string-hex`
    Hex encode or decode strings, and also use hex dumps for debugging.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   string-utf8
   string-util
   string-urlencode
   string-hex

System Utiliities
=================

:doc:`sys-error`
    Deal with system error codes in a platform independent way.

:doc:`sys-memory`
    Write memory in a platform independent way.

:doc:`sys-pid`
    Access process IDs in a platform independent way.

:doc:`sys-plugin`
    Write cross-platform plugins.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   sys-error
   sys-memory
   sys-pid
   sys-plugin

Concurrency
===========

:doc:`concurrency-tasklet`
    Using tasklets to simplify thread management.

:doc:`concurrency-queue`
    Using a concurrent queue for sharing data between threads.

:doc:`concurrency-command`
    Dispatch commands to worker threads and fetch results.

:doc:`concurrency-lock-policy`
    Use lock policies to write code that *may* use locks, but can also be used
    lock free.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   concurrency-tasklet
   concurrency-queue
   concurrency-command
   concurrency-lock-policy

Miscellaneous
=============

:doc:`checksum-crc32`
    Calculate and verify CRC32 checksums.

:doc:`random-unsafe-bits`
    Generate random bits in a cryptographically unsafe way.

:doc:`timeout-exponential-backoff`
    Generate exponential backoff series for timeouts.

:doc:`logging`
    Use logging at various log levels in your code and choose your preferred
    logging backend.

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:
   :includehidden:

   checksum-crc32
   random-unsafe-bits
   timeout-exponential-backoff
   logging
