==========================================
How-To perform common string manipulations
==========================================

The `liberate/string/util.h` header contains assorted string manipulation
functions. This collection is far from comprehensive, but it does provide
functionality not offered by the standard library without some extra work.

Upper-/lower-case strings
=========================

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Upper-/lower-case strings

    assert("asdf" == liberate::string::to_lower("AsDf"));
    assert("ASDF" == liberate::string::to_upper("AsDf"));

.. seealso::
   - :cpp:func:`liberate::string::to_lower`
   - :cpp:func:`liberate::string::to_upper`


Replace parts of a string
=========================

The example replaces spaces with underscores. Note that it's perfectly possible
to use longer needles and substitutes.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Replace occurrences of a string with something else

    auto result = liberate::string::replace(
        "The quick brown fox jumped over the lazy dog.",
        " ", "_");
    // => "The_quick_brown_fox_jumped_over_the_lazy_dog."

.. seealso::
   - :cpp:func:`liberate::string::replace`

Find a substring in a string
============================

Related to the above, we can find a needle in a haystack in a case insensitive
manner.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Find a string (case insensitive)

    auto result = liberate::string::ifind(
        "The QUICk brown fox jumped over the lazy dog.",
        "QUIC");
    assert(result == 4);

    result = liberate::string::ifind(
        "The QUICk brown fox jumped over the lazy dog.",
        "foo");
    assert(result == -1);

.. seealso::
   - :cpp:func:`liberate::string::ifind`

Split a string along delimiters
===============================

We can also split a string along some delimiters. By default, this splits
along whitespace, but we can use any collection of delimiters.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Split a string

    auto result = liberate::string::split(
        "Hello, world!", ", !");
    assert(2 == result.size());
    assert(result[0] == "Hello");
    assert(result[1] == "world");

.. seealso::
   - :cpp:func:`liberate::string::split`

