============================
How-To create random numbers
============================

This part of liberate mostly takes the annoying boilerplate code out of standard
library functions. In particular, it provides a simple way to generate random
numbers using ``std::default_random_engine`` and ``std::uniform_int_distribution``
as its building blocks.

It assumes that what we want most of the time is to generate random numbers of
some integer type ``T``, within a range of valid values for that type. For
example, we might want to pick ``uint16_t`` in the range of ``40`` to ``300``.

We initialize the ``unsafe_bits`` structure with those parameters, and then
can generate as many random numbers from that as we'd like.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Generate random numbers

    #include <liberate/random/unsafe_bits.h>

    liberate::random::unsafe_bits<uint16_t> rnd{40, 300};

    auto val = rnd.get(); // <= random uint16_t in the range [40, 300]
