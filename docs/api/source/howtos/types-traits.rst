===============================
How-To use extended type traits
===============================

The library provides a bunch of type traits beyond what exists in the standard
library that may or may not be useful to you.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Using size traits

    #include <liberate/types/type_traits.h>

    using namespace liberate::types;

    assert(is_8bit_type<char>::value);
    assert(is_16bit_type<uint16_t>::value);
    assert(is_32bit_type<uint32_t>::value);
    assert(is_64bit_type<uint64_t>::value);
    assert(is_128bit_type<uint128_t>::value);
