=======================
How-To manage a network
=======================

Using the :cpp:class:`liberate::net::network` class, you can manage the
assignment of individual addresses in the network. Typically, when that is
necessary, each address serves a particular *purpose*, which we can assign
ourselves.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Create a network from CIDR-style notation

    liberate::net::network net{"192.168.0.0/16"};

    auto addr = net.reserve_address("my first purpose");

The resulting address will *not* be the first address in the network, but
rather some random-seeming address from the available range. As a matter of
fact, the class hashes the input ``identifier`` string, and picks an address
from the network that corresponds to the hash value. This means that for two
invocations with the same identifier, the same address should be returned.

That is, however, not quite the case. The ``reserve_address()`` function not
only performs the mapping, but also makes the reservation, so that a subsequent
reservation with the same identifier fails. You can, however, release the
reservation before making it again.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Matching between identifiers and addresses

    liberate::net::network net{"192.168.0.0/16"};

    auto addr1 = net.reserve_address("purpose");
    net.release_address(addr1);

    auto addr2 = net.reserve_address("purpose");
    assert(addr1 == addr2);

Of course, you can simply reserve addresses sequentially.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Matching between identifiers and addresses

    liberate::net::network net{"192.168.0.0/16"};

    auto addr = net.gateway_address();
    net.reserve_address(addr); // => true

    ++addr;
    net.reserve_address(addr); // => true

    // and so forth
