=======================
How-To parse IP headers
=======================

A slightly more esoteric topic when it comes to IPv4 and IPv6 networking is
to parse respective address types from IP packet headers. However, it may
come in handy when e.g. using UDP sockets that are not bound to any particular
corresponding peer address.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Parse an IPv4 or IPv6 packet header

    #include <liberate/net/ip.h>

    using namespace liberate::net;
    socket_address src;
    socket_address dest;

    auto res = parse_addresses(src, dest, ip_buffer, ip_buffer_size);

The function returns false if parsing failed, true otherwise. As a side
effect of the function, the ``type()`` of the returned addresses also tells you
the type of IP packet/header.

.. seealso::
   :cpp:func:`liberate::net::parse_addresses`
