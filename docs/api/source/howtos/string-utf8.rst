==============================
How-To work with UTF8 encoding
==============================

The Windows APIs often treat strings as UCS-2 encoded, whereas most POSIX APIs
expect UTF-8. The library provides simple functions for converting between the
two encodings.

.. warning::
   These functions only exist for the Win32 plattform.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Transcode between UCS-2 and UTF-8

    #include <liberate/string/utf8.h>

    auto utf = liberate::string::to_utf8(/* some UCS-2 input as a TCHAR pointer */);
    auto back = liberate::string::from_utf8(utf);

.. seealso::
   - :cpp:func:`liberate::string::to_utf8`
   - :cpp:func:`liberate::string::from_utf8`
