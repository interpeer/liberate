===============================================
How-To parse CIDR addresses into socket_address
===============================================

The :cpp:class:`liberate::net::socket_address` class also permits easy
construction from string representations (in CIDR-style notation). We can
optionally add a port.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Create from string

    liberate::net::socket_address addr{"192.168.0.1", 443};

    assert(address_type::AF_INET4 == addr.type());

From here, we can convert the address back, or access the port, etc.

 .. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Access port, string representation

    assert(443 == addr.port());
    assert(addr.cidr_str() == "192.168.0.1");
    assert(addr.full_str() == "192.168.0.1:443");
