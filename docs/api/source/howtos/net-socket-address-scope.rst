=======================================
How-To determine a socket address scope
=======================================

Using :cpp:class:`liberate::net::socket_address` often means understanding
something about the scope of the address.

For binding to a socket, for example, it may be useful to bind to ``INADDR_ANY``
or its IPv6 equivalent. Across both IPv4 and IPv6, you can use the same function
to determine whether the address is that special "any" designator.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Determine if the address is INADDR_ANY

    if (addr.is_any()) { /* ... */ }

Similarly, both IPv4 and IPv6 defines a loopback address. You can find out
equally easily if that is what you are dealing with.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Determine if you have a loopback address

    if (addr.is_loopback()) { /* ... */ }

Finally, it may be useful to understand whether a network mask can be reasonably
applied to a socket address.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Create from string

    liberate::net::socket_address addr{"192.168.0.1", 443};

    // This address works with a /24 mask
    assert(addr.verify_netmask(24));

    // But it cannot work with a /42 mask
    assert(!addr.verify_netmask(42));
