====================================
How-To deal with process identifiers
====================================

You need to get an identifier for the current process? You can have it.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Platform errors

    #include <liberate/sys/pid.h>

    auto pid = liberate::sys::getpid();

Yes, it's kind of annoying that this needs system dependent code. But here
we are, and this is why this function exists.
