=====================
How-To work with URLs
=====================

The library provides *very simple* URL parsing via the
:cpp:class:`liberate::net::url` class.

.. warning::
   The URL parsing provided here is **in no way standards conforming**. It
   exists soley to have a uniform syntax for simple things like socket addresses
   that can also span local AF_UNIX sockets.

.. sourcecode:: cpp
   :linenos:
   :dedent:
   :caption: Parse a URL

    #include <liberate/net/url.h>

    liberate::net::url url{"https://example.com/foo/bar?asdf=1#blah"};

    assert(url.scheme == "https");
    assert(url.authority == "example.com");
    assert(url.path == "/foo/bar");
    auto iter = url.query.find("asdf");
    assert(iter != url.query.end());
    assert(iter->second == "1");
    assert(url.fragment == "blah");

URLs are hashable and comparable.

.. seealso::
   - :doc:`cpp-comparison`
   - :doc:`cpp-hashing`
   - :doc:`string-urlencode`


