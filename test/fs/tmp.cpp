/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/fs/tmp.h>

#include <fstream>

#include <gtest/gtest.h>


TEST(FsTmp, has_value)
{
  auto tmp = liberate::fs::temp_name();
  // std::cout << "TMP: "<< tmp << std::endl;

  // Ask for a few characters at least
  ASSERT_GT(tmp.size(), 3);
}



TEST(FsTmp, contains_prefix)
{
  auto tmp = liberate::fs::temp_name("foo");
  // std::cout << "TMP: "<< tmp << std::endl;
  EXPECT_GT(tmp.size(), 3);

  auto pos = tmp.find("foo");
  ASSERT_NE(pos, std::string::npos);
}


TEST(FsTmp, open_file)
{
  auto tmp = liberate::fs::temp_name("foo");
  std::ofstream os(tmp.c_str());
  os << "test";
  os.close();
}
