/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/sys/plugin.h>

#include "plugin_src.h"

std::string hello_world_impl()
{
  return "Hello from the plugin!";
}

plugin::plugin_data my_data = { hello_world_impl };

liberate::sys::plugin_meta plugin_meta =
{
  0x42, 0xdeadd00d, &my_data, nullptr,
};

extern "C" {

#if defined(_WIN32) || defined(__CYGWIN__) || defined(__MINGW32__)
  #define PLUGIN_EXPORT __declspec(dllexport)
#else
  #define PLUGIN_EXPORT
#endif

PLUGIN_EXPORT void *
liberate_plugin_meta()
{
  return &plugin_meta;
}

} // extern "C"
