/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/sys/plugin.h>

#include <gtest/gtest.h>

#include "plugin_src.h"

#if defined(LIBERATE_POSIX)
auto constexpr PLUGIN_FILENAME = "test/libplugin.so";
#else
auto constexpr PLUGIN_FILENAME = "test\\plugin.dll";
#endif

TEST(Plugin, basics)
{
  using namespace liberate::sys;

  plugin_meta const * meta = nullptr;

  // Load plugin
  auto err = load_plugin(meta, PLUGIN_FILENAME);
  ASSERT_TRUE(err);

  // Test plugin data
  ASSERT_TRUE(meta);
  ASSERT_EQ(meta->type, 0x42);
  ASSERT_EQ(meta->version, 0xdeadd00d);
  ASSERT_TRUE(meta->data);
  ASSERT_TRUE(meta->plugin_handle);

  // Ok, so we should be able to cast the data to the plugin data structure
  auto plg = static_cast<plugin::plugin_data *>(meta->data);

  // And invoke functions
  auto res = plg->hello_world();
  ASSERT_EQ(res, "Hello from the plugin!");

  // Unload
  err = unload_plugin(meta);
  ASSERT_TRUE(err);
}
