/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/sys/memory.h>

#include <gtest/gtest.h>

TEST(SysMemory, secure_memset)
{
  char buf[] = "Some test data.";
  liberate::sys::secure_memset(buf, sizeof(buf), 42);
  for (size_t i = 0 ; i < sizeof(buf) ; ++i) {
    ASSERT_EQ(42, buf[i]);
  }
}


TEST(SysMemory, secure_memzero)
{
  char buf[] = "Some test data.";
  liberate::sys::secure_memzero(buf, sizeof(buf));
  for (size_t i = 0 ; i < sizeof(buf) ; ++i) {
    ASSERT_EQ(0, buf[i]);
  }
}
