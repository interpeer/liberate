/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/cpp/operators.h>

#include <gtest/gtest.h>

struct foo : public liberate::cpp::comparison_operators<foo>
{
  int a;

  foo(int _a)
    : liberate::cpp::comparison_operators<foo>{}
    , a(_a)
  {
  }

  bool is_equal_to(foo const & other) const
  {
    return a == other.a;
  }

  bool is_less_than(foo const & other) const
  {
    return a < other.a;
  }
};



TEST(CppOperatorsComparison, test_equality)
{
  foo f1{42};
  foo f2{42};

  ASSERT_EQ(f1.a, 42);
  ASSERT_EQ(f1, f2);
}


TEST(CppOperatorsComparison, test_inequality)
{
  foo f1{41};
  foo f2{42};

  ASSERT_NE(f1, f2);
}


TEST(CppOperatorsComparison, test_less_than)
{
  foo f1{41};
  foo f2{42};

  ASSERT_LT(f1, f2);
}


TEST(CppOperatorsComparison, test_less_equal)
{
  foo f1{41};
  foo f2{42};
  foo f3{42};

  ASSERT_LE(f1, f2);
  ASSERT_LE(f2, f3);
}



TEST(CppOperatorsComparison, test_greater_than)
{
  foo f1{43};
  foo f2{42};

  ASSERT_GT(f1, f2);
}


TEST(CppOperatorsComparison, test_greater_equal)
{
  foo f1{43};
  foo f2{42};
  foo f3{42};

  ASSERT_GE(f1, f2);
  ASSERT_GE(f2, f3);
}
