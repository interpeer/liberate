/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <cstddef>

#include <liberate/serialization/integer.h>
#include <liberate/serialization/varint.h>
#include <liberate/string/hexencode.h>

#include <gtest/gtest.h>

TEST(SerializationVarint, serialize_to_uint8_t)
{
  using namespace liberate::types::literals;
  using namespace liberate::types;
  using namespace liberate::serialization;

  uint8_t out[20] = { 0 };

  auto test = 0x01020304_var;

  // The following is not enabled.
  // auto written = serialize_int(&out[0], sizeof(out), test);

  // This, however, should work.
  auto written = serialize_varint(&out[0], sizeof(out), test);

  ASSERT_EQ(written, 4);

  ASSERT_EQ(out[0], 0x84);
  ASSERT_EQ(out[1], 0x86);
  ASSERT_EQ(out[2], 0x88);
  ASSERT_EQ(out[3], 0x08);

  // A small value should be encoded in one byte.
  test = 42_var;
  written = serialize_varint(&out[0], sizeof(out), test);
  ASSERT_EQ(written, 1);
  ASSERT_EQ(out[0], 42);
}


TEST(SerializationVarint, serialize_to_byte)
{
  using namespace liberate::types::literals;
  using namespace liberate::types;
  using namespace liberate::serialization;

  std::byte out[20] = { std::byte{0} };

  auto test = 0x01020304_var;

  // The following is not enabled.
  // auto written = serialize_int(&out[0], sizeof(out), test);

  // This, however, should work.
  auto written = serialize_varint(&out[0], sizeof(out), test);

  ASSERT_EQ(written, 4);

  ASSERT_EQ(out[0], static_cast<std::byte>(0x84));
  ASSERT_EQ(out[1], static_cast<std::byte>(0x86));
  ASSERT_EQ(out[2], static_cast<std::byte>(0x88));
  ASSERT_EQ(out[3], static_cast<std::byte>(0x08));

  // A small value should be encoded in one byte.
  test = 42_var;
  written = serialize_varint(&out[0], sizeof(out), test);
  ASSERT_EQ(written, 1);
  ASSERT_EQ(out[0], static_cast<std::byte>(42));
}


TEST(SerializationVarint, deserialize_from_uint8_t)
{
  using namespace liberate::types::literals;
  using namespace liberate::types;
  using namespace liberate::serialization;

  uint8_t in[] = { 0x84, 0x86, 0x88, 0x08, 0xde, 0xad };

  varint result;
  auto read = deserialize_varint(result, in, sizeof(in));

  ASSERT_EQ(read, 4);
  ASSERT_EQ(result, 0x01020304_var);

  // A small value should be encoded in one byte.
  uint8_t in2[] = { 42, 0xde, 0xad };
  read = deserialize_varint(result, in2, sizeof(in2));
  ASSERT_EQ(read, 1);
  ASSERT_EQ(result, 42_var);
}


TEST(SerializationVarint, deserialize_from_byte)
{
  using namespace liberate::types::literals;
  using namespace liberate::types;
  using namespace liberate::serialization;

  std::byte in[] = {
    static_cast<std::byte>(0x84),
    static_cast<std::byte>(0x86),
    static_cast<std::byte>(0x88),
    static_cast<std::byte>(0x08),
    static_cast<std::byte>(0xde),
    static_cast<std::byte>(0xad),
  };

  varint result;
  auto read = deserialize_varint(result, in, sizeof(in));

  ASSERT_EQ(read, 4);
  ASSERT_EQ(result, 0x01020304_var);

  // A small value should be encoded in one byte.
  std::byte in2[] = {
    static_cast<std::byte>(42),
    static_cast<std::byte>(0xde),
    static_cast<std::byte>(0xad),
  };
  read = deserialize_varint(result, in2, sizeof(in2));
  ASSERT_EQ(read, 1);
  ASSERT_EQ(result, 42_var);
}



TEST(SerializationVarint, deserialize_buffer_too_small)
{
  using namespace liberate::types::literals;
  using namespace liberate::types;
  using namespace liberate::serialization;

  uint8_t in[] = { 0x87, 0x87 };

  varint result;
  auto read = deserialize_varint(result, in, sizeof(in));

  ASSERT_EQ(read, 0);
}


TEST(SerializationVarint, serialize_buffer_too_small)
{
  using namespace liberate::types::literals;
  using namespace liberate::types;
  using namespace liberate::serialization;

  uint8_t out[2] = { 0 };

  auto test = 0x01020304_var;

  // The following is not enabled.
  // auto written = serialize_int(&out[0], sizeof(out), test);

  // This, however, should work.
  auto written = serialize_varint(out, sizeof(out), test);

  ASSERT_EQ(written, 0);
}


TEST(SerializationVarint, buffer_excessively_large)
{
  using namespace liberate::types::literals;
  using namespace liberate::types;
  using namespace liberate::serialization;

  uint8_t buf[30] = { 0 };

  auto test = 0x1a_var;

  // Write to buffer. We would expect it to be a single byte large, and
  // that byte to sit in the first byte of the buffer.
  auto written = serialize_varint(buf, sizeof(buf), test);
  ASSERT_EQ(written, 1);
  ASSERT_EQ(buf[0], 0x1a);
  ASSERT_EQ(buf[1], 0x00);
  ASSERT_EQ(buf[29], 0x00);

  // Deserialization of the same large buffer must also work as expected.
  varint result;
  auto read = deserialize_varint(result, buf, sizeof(buf));
  ASSERT_EQ(read, 1);
  ASSERT_EQ(result, test);
}


namespace {

struct test_data
{
  int64_t             input;
  size_t              expected_size;
  char const * const  data;
};

test_data sleb128_tests[] = {
  // Examples from https://dwarfstd.org/DownloadDwarf5.php (SLEB128 definition)
  {    2, 1, "\x02", },
  {   -2, 1, "\x7e", },
  {  127, 2, "\xff\x00", },
  { -127, 2, "\x81\x7f", },
  {  128, 2, "\x80\x01", },
  { -128, 2, "\x80\x7f", },
  {  129, 2, "\x81\x01", },
  { -129, 2, "\xff\x7e", },

  // Additional examples
  { 0, 1, NULL },
  { 42, 1, NULL },
  { 63, 1, NULL },
  { 64, 2, NULL },
  { 76, 2, NULL },
  { 126, 2, NULL },
  { 255, 2, NULL },
  { 300, 2, NULL },
  { 16383, 3, NULL },
  { 16384, 3, NULL },
  { std::numeric_limits<int16_t>::max() - 1, 3, NULL },
  { std::numeric_limits<int16_t>::max(), 3, NULL },
  { std::numeric_limits<uint16_t>::max() - 1, 3, NULL },
  { std::numeric_limits<uint16_t>::max(), 3, NULL },
  { std::numeric_limits<int32_t>::max() - 1, 5, NULL },
  { std::numeric_limits<int32_t>::max(), 5, NULL },
  { std::numeric_limits<uint32_t>::max() - 1, 5, NULL },
  { std::numeric_limits<uint32_t>::max(), 5, NULL },
  { std::numeric_limits<int64_t>::max() - 1, 10, NULL },
  { std::numeric_limits<int64_t>::max(), 10, NULL },

  { -42, 1, NULL },
  { std::numeric_limits<int16_t>::min(), 3, NULL },
  { std::numeric_limits<int32_t>::min(), 5, NULL },
  { std::numeric_limits<int64_t>::min(), 10, NULL },
};



test_data uleb128_tests[] = {
  // Examples from https://dwarfstd.org/DownloadDwarf5.php (ULEB128 definition)
  {     2, 1, "\x02", },
  {   127, 1, "\x7f", },
  {   128, 2, "\x80\x01", },
  {   129, 2, "\x81\x01", },
  { 12857, 2, "\xb9\x64", },

  // Additional examples
  { 0, 1, NULL },
  { 42, 1, NULL },
  { 63, 1, NULL },
  { 64, 1, NULL },
  { 76, 1, NULL },
  { 126, 1, NULL },
  { 255, 2, NULL },
  { 300, 2, NULL },
  { 16383, 2, NULL },
  { 16384, 3, NULL },
  { std::numeric_limits<int16_t>::max(), 3, NULL },
  { std::numeric_limits<uint16_t>::max(), 3, NULL },
  { std::numeric_limits<int32_t>::max(), 5, NULL },
  { std::numeric_limits<uint32_t>::max(), 5, NULL },
  { std::numeric_limits<int64_t>::max(), 9, NULL },

  // A negative number
  { -1, 0, NULL, },
  { std::numeric_limits<int16_t>::min(), 0, NULL },
  { std::numeric_limits<int32_t>::min(), 0, NULL },
  { std::numeric_limits<int64_t>::min(), 0, NULL },

};


std::string generate_name(testing::TestParamInfo<test_data> const & info)
{
  if (info.param.input < 0) {
    int64_t flipped = -1 * info.param.input;
    if (flipped < 0) {
      return "minux_max";
    }
    return "minus_" + std::to_string(flipped);
  }
  return std::to_string(info.param.input);
}

} // anonymous namespace

class VarIntSLEB128
  : public testing::TestWithParam<test_data>
{
};


TEST_P(VarIntSLEB128, test_serialize_deserialize)
{
  auto td = GetParam();

  using namespace liberate::types;
  using namespace liberate::serialization;

  varint tmp = static_cast<varint>(td.input);

  char buf[20];
  auto used = serialize_varint(buf, sizeof(buf), tmp);
  ASSERT_EQ(used, td.expected_size);
  ASSERT_EQ(serialized_size(tmp), used);

#if 0
  std::cerr << "---- serialize: " << used << std::endl;
  auto hd = liberate::string::hexencode(buf, used);
  std::cerr << "hex:      " << hd << std::endl;
  if (td.data) {
    hd = liberate::string::hexencode(td.data, td.expected_size);
    std::cerr << "expected: " << hd << std::endl;
  }
#endif

  if (td.data) {
    ASSERT_EQ(0, std::memcmp(buf, td.data, used));
  }

  varint tmp2;
  auto used2 = deserialize_varint(tmp2, buf, used);
  ASSERT_EQ(used, used2);

  ASSERT_EQ(tmp, tmp2);
  ASSERT_EQ(static_cast<uint64_t>(tmp2), td.input);

}


INSTANTIATE_TEST_SUITE_P(serialization, VarIntSLEB128, testing::ValuesIn(sleb128_tests),
    generate_name);


class VarIntULEB128
  : public testing::TestWithParam<test_data>
{
};


TEST_P(VarIntULEB128, test_serialize_deserialize)
{
  auto td = GetParam();

  using namespace liberate::types;
  using namespace liberate::serialization;

  varint tmp = static_cast<varint>(td.input);

  char buf[20];
  auto used = uleb128_serialize_varint(buf, sizeof(buf), tmp);
  ASSERT_EQ(used, td.expected_size);
  if (!td.expected_size) {
    return;
  }

  ASSERT_EQ(uleb128_serialized_size(tmp), used);

#if 0
  std::cerr << "---- serialize: " << used << std::endl;
  auto hd = liberate::string::hexencode(buf, used);
  std::cerr << "hex:      " << hd << std::endl;
  if (td.data) {
    hd = liberate::string::hexencode(td.data, td.expected_size);
    std::cerr << "expected: " << hd << std::endl;
  }
#endif

  if (td.data) {
    ASSERT_EQ(0, std::memcmp(buf, td.data, used));
  }

  varint tmp2;
  auto used2 = uleb128_deserialize_varint(tmp2, buf, used);
  ASSERT_EQ(used, used2);

  ASSERT_EQ(tmp, tmp2);
  ASSERT_EQ(static_cast<uint64_t>(tmp2), td.input);

}


INSTANTIATE_TEST_SUITE_P(serialization, VarIntULEB128, testing::ValuesIn(uleb128_tests),
    generate_name);
