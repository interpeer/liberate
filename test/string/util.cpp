/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/string/util.h>

#include <gtest/gtest.h>


TEST(StringUtil, to_lower)
{
  namespace s = liberate::string;

  ASSERT_EQ("foo", s::to_lower("foo"));
  ASSERT_EQ("foo", s::to_lower("Foo"));
  ASSERT_EQ("foo", s::to_lower("fOo"));
  ASSERT_EQ("foo", s::to_lower("foO"));
  ASSERT_EQ("foo", s::to_lower("FOO"));

  ASSERT_EQ("", s::to_lower(""));
  ASSERT_EQ("a", s::to_lower("A"));
}


TEST(StringUtil, to_upper)
{
  namespace s = liberate::string;

  ASSERT_EQ("FOO", s::to_upper("foo"));
  ASSERT_EQ("FOO", s::to_upper("Foo"));
  ASSERT_EQ("FOO", s::to_upper("fOo"));
  ASSERT_EQ("FOO", s::to_upper("foO"));
  ASSERT_EQ("FOO", s::to_upper("FOO"));

  ASSERT_EQ("", s::to_upper(""));
  ASSERT_EQ("A", s::to_upper("a"));
}


TEST(StringUtil, case_insensitive_search)
{
  namespace s = liberate::string;

  auto res = s::ifind("This is a Test String", "test");
  ASSERT_GE(res, 0); // Less than zero would be failure

  // "test" is at offset 10
  ASSERT_EQ(10, res);

  // Find something at the beginning and end of strings
  ASSERT_EQ(0, s::ifind("foobar", "FOO"));
  ASSERT_EQ(3, s::ifind("foobar", "Bar"));

  // Return -1 if string can't be found
  ASSERT_EQ(-1, s::ifind("foobar", "quux"));
  ASSERT_EQ(-1, s::ifind("quu", "quux"));
  ASSERT_EQ(-1, s::ifind("", "quux"));

  // Find the empty string anywhere, so at 0.
  ASSERT_EQ(0, s::ifind("foobar", ""));
  // .. except in an empty string
  ASSERT_EQ(-1, s::ifind("", ""));
}


TEST(StringUtil, replace)
{
  namespace s = liberate::string;

  ASSERT_EQ("foo", s::replace("f0o", "0", "o"));
  ASSERT_EQ("fo0", s::replace("f00", "0", "o", true));

  ASSERT_EQ("\\\\quoted\\\\and\\\\separated\\\\",
      s::replace("\\quoted\\and\\separated\\", "\\", "\\\\"));
}


TEST(StringUtil, split)
{
  namespace s = liberate::string;

  // Default delimiters
  {
    auto res = s::split("the quick\tbrown fox\njumped over the lazy dog!");
    ASSERT_EQ(9, res.size());
    ASSERT_EQ("the", res[0]);
    ASSERT_EQ("quick", res[1]);
    ASSERT_EQ("brown", res[2]);
    ASSERT_EQ("fox", res[3]);
    ASSERT_EQ("jumped", res[4]);
    ASSERT_EQ("over", res[5]);
    ASSERT_EQ("the", res[6]);
    ASSERT_EQ("lazy", res[7]);
    ASSERT_EQ("dog!", res[8]);
  }

  // Multiple delimiters in a row
  {
    auto res = s::split("foo \t\n bar");
    ASSERT_EQ(2, res.size());
    ASSERT_EQ("foo", res[0]);
    ASSERT_EQ("bar", res[1]);
  }

  // Custom delimiters
  {
    auto res = s::split("Hello, world!", ",;");
    ASSERT_EQ(2, res.size());
    ASSERT_EQ("Hello", res[0]);
    ASSERT_EQ(" world!", res[1]);
  }
}
