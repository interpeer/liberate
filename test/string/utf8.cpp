/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/string/utf8.h>

#include <gtest/gtest.h>

#if defined(LIBERATE_WIN32)

#if defined(LIBERATE_BIGENDIAN)
#  error Define test_data appropriately
#else
TCHAR const test_data[] {
  TCHAR{'h'},
  TCHAR{'e'},
  TCHAR{'l'},
  TCHAR{'l'},
  TCHAR{'o'}
};
#endif

TEST(StringUTF8, ucs2_to_utf8)
{
  std::string res = liberate::string::to_utf8(test_data);
  ASSERT_EQ(res, std::string{"hello"});
}

TEST(StringUTF8, utf8_to_ucs2)
{
  std::wstring res = liberate::string::from_utf8("hello");
  ASSERT_EQ(res, std::wstring{test_data});
}

#endif // LIBERATE_WIN32
