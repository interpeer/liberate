/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/string/urlencode.h>

#include <gtest/gtest.h>


TEST(StringURLDecode, urlencode)
{
  namespace s = liberate::string;

  ASSERT_EQ("foo/bar", s::urlencode("foo/bar"));
  ASSERT_EQ("/%7Efoo/bar", s::urlencode("/~foo/bar"));
  ASSERT_EQ("%00abstract", s::urlencode(std::string{"\0abstract", 9}));
  ASSERT_EQ("%25asdf", s::urlencode("%asdf"));
}



TEST(StringURLDecode, urldecode)
{
  namespace s = liberate::string;

  ASSERT_EQ(s::urldecode("foo/bar"), "foo/bar");
  ASSERT_EQ(s::urldecode("/%7Efoo/bar"), "/~foo/bar");
  ASSERT_EQ(s::urldecode("%00abstract"), (std::string{"\0abstract", 9}));
  ASSERT_EQ(s::urldecode("%25asdf"), "%asdf");
}
