/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <liberate/net/resolve.h>

#include <gtest/gtest.h>

#include "../test_name.h"
#include "../env.h"


#define INTERPEER_IO_V4 "88.99.83.249"
#define INTERPEER_IO_V6 "2a01:4f8:c17:85e4::1"

namespace net = liberate::net;

TEST(Resolve, localhost_any)
{
  // The difficulty here is that we may have one or two results,
  // depending on how the test host is connected. Still, that's not
  // actually bad.
  // We could also have zero in an entirely non-networked situation.
  auto result = net::resolve(test_env->api, net::AT_UNSPEC, "localhost");
  if (result.empty()) {
    GTEST_SKIP() << "Perhaps the test host has no network?";
  }
  ASSERT_TRUE(result.size() == 1 || result.size() == 2);

  for (auto addr : result) {
    if (addr.type() == net::AT_INET4) {
      ASSERT_EQ(addr.cidr_str(), "127.0.0.1");
    }
    else if (addr.type() == net::AT_INET6) {
      ASSERT_EQ(addr.cidr_str(), "::1");
    }
  }
}


TEST(Resolve, localhost_ipv4_with_port)
{
  auto result = net::resolve(test_env->api, net::AT_INET4, "localhost:1234");
  ASSERT_EQ(result.size(), 1);
  auto addr = *result.begin();
  ASSERT_EQ(addr.type(), net::AT_INET4);
  ASSERT_EQ(1234, addr.port());
  ASSERT_EQ(addr.cidr_str(), "127.0.0.1");
}


TEST(Resolve, test_domain_error)
{
  // As per specs, domains ending in .test should never be resolvable.
  auto result = net::resolve(test_env->api, net::AT_UNSPEC, "resolve.test");
  ASSERT_TRUE(result.empty());
}


TEST(Resolve, interpeer_any)
{
  // If the interpeer configuration does not change, this should yield IPv4
  // and IPv6 addresses
  auto result = net::resolve(test_env->api, net::AT_UNSPEC, "interpeer.io");
  if (result.empty()) {
    GTEST_SKIP() << "Perhaps the test host has no network?";
  }
  for (auto addr : result) {
    if (addr.type() == net::AT_INET4) {
      ASSERT_EQ(addr.cidr_str(), INTERPEER_IO_V4);
    }
    else if (addr.type() == net::AT_INET6) {
      ASSERT_EQ(addr.cidr_str(), INTERPEER_IO_V6);
    }
  }
}

TEST(Resolve, interpeer_ipv4)
{
  auto result = net::resolve(test_env->api, net::AT_INET4, "interpeer.io");
  if (result.empty()) {
    GTEST_SKIP() << "Perhaps the test host has no network?";
  }
  ASSERT_EQ(result.size(), 1);
  auto addr = *result.begin();
  ASSERT_EQ(addr.type(), net::AT_INET4);
  ASSERT_EQ(addr.cidr_str(), INTERPEER_IO_V4);
}

TEST(Resolve, interpeer_ipv6)
{
  auto result = net::resolve(test_env->api, net::AT_INET6, "interpeer.io");
  if (result.empty()) {
    GTEST_SKIP() << "Perhaps the test host has no network?";
  }

  ASSERT_EQ(result.size(), 1);
  auto addr = *result.begin();
  ASSERT_EQ(addr.type(), net::AT_INET6);
  ASSERT_EQ(addr.cidr_str(), INTERPEER_IO_V6);
}
