/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <liberate/net/ip.h>

#include <gtest/gtest.h>

#include "../test_name.h"

namespace net = liberate::net;

namespace {

using byte = liberate::types::byte;

// Two packets grabbed with wireshark
constexpr byte const ipv4_buf[] = {
    byte{0x45}, byte{0x00}, byte{0x00}, byte{0x28},
    byte{0x00}, byte{0x00}, byte{0x40}, byte{0x00},
    byte{0x40}, byte{0x06}, byte{0x3c}, byte{0xce},
    byte{0x7f}, byte{0x00}, byte{0x00}, byte{0x01},
    byte{0x7f}, byte{0x00}, byte{0x00}, byte{0x01}
  };


constexpr byte const ipv6_buf[] = {
    byte{0x60}, byte{0x03}, byte{0x9b}, byte{0xe1},
    byte{0x00}, byte{0x14}, byte{0x06}, byte{0x40},
    byte{0x00}, byte{0x00}, byte{0x00}, byte{0x00},
    byte{0x00}, byte{0x00}, byte{0x00}, byte{0x00},
    byte{0x00}, byte{0x00}, byte{0x00}, byte{0x00},
    byte{0x00}, byte{0x00}, byte{0x00}, byte{0x01},
    byte{0x00}, byte{0x00}, byte{0x00}, byte{0x00},
    byte{0x00}, byte{0x00}, byte{0x00}, byte{0x00},
    byte{0x00}, byte{0x00}, byte{0x00}, byte{0x00},
    byte{0x00}, byte{0x00}, byte{0x00}, byte{0x01}
  };

} // anonymous namespace


TEST(IP, parse_source_address_ipv4)
{
  net::socket_address addr;
  auto res = net::parse_source_address(addr, ipv4_buf, sizeof(ipv4_buf));
  ASSERT_TRUE(res);
  ASSERT_EQ(addr.type(), net::AT_INET4);
  ASSERT_EQ(addr, net::socket_address{"127.0.0.1"});
}



TEST(IP, parse_source_address_ipv6)
{
  net::socket_address addr;
  auto res = net::parse_source_address(addr, ipv6_buf, sizeof(ipv6_buf));
  ASSERT_TRUE(res);
  ASSERT_EQ(addr.type(), net::AT_INET6);
  ASSERT_EQ(addr, net::socket_address{"::1"});
}



TEST(IP, parse_dest_address_ipv4)
{
  net::socket_address addr;
  auto res = net::parse_dest_address(addr, ipv4_buf, sizeof(ipv4_buf));
  ASSERT_TRUE(res);
  ASSERT_EQ(addr.type(), net::AT_INET4);
  ASSERT_EQ(addr, net::socket_address{"127.0.0.1"});
}



TEST(IP, parse_dest_address_ipv6)
{
  net::socket_address addr;
  auto res = net::parse_dest_address(addr, ipv6_buf, sizeof(ipv6_buf));
  ASSERT_TRUE(res);
  ASSERT_EQ(addr.type(), net::AT_INET6);
  ASSERT_EQ(addr, net::socket_address{"::1"});
}



TEST(IP, parse_addresses_ipv4)
{
  net::socket_address source;
  net::socket_address dest;
  auto res = net::parse_addresses(source, dest, ipv4_buf, sizeof(ipv4_buf));
  ASSERT_TRUE(res);
  ASSERT_EQ(source.type(), net::AT_INET4);
  ASSERT_EQ(source, net::socket_address{"127.0.0.1"});
  ASSERT_EQ(dest.type(), net::AT_INET4);
  ASSERT_EQ(dest, net::socket_address{"127.0.0.1"});
}


TEST(IP, parse_addresses_ipv6)
{
  net::socket_address source;
  net::socket_address dest;
  auto res = net::parse_addresses(source, dest, ipv6_buf, sizeof(ipv6_buf));
  ASSERT_TRUE(res);
  ASSERT_EQ(source.type(), net::AT_INET6);
  ASSERT_EQ(source, net::socket_address{"::1"});
  ASSERT_EQ(dest.type(), net::AT_INET6);
  ASSERT_EQ(dest, net::socket_address{"::1"});
}


TEST(IP, parse_bad_packet)
{
  net::socket_address addr;
  auto res = net::parse_source_address(addr,
      reinterpret_cast<byte const *>("Hello, world!"), 13);
  ASSERT_FALSE(res);
}
