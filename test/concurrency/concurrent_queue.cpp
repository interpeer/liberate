/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/concurrency/concurrent_queue.h>

#include <gtest/gtest.h>

namespace lc = liberate::concurrency;


TEST(ConcurrentQueue, queue_functionality)
{
  // Test that the FIFO principle works for the queue as such, and that
  // extra functions do what they're supposed to do.
  lc::concurrent_queue<int> queue;
  ASSERT_EQ(true, queue.empty());
  ASSERT_EQ(0, queue.size());

  queue.push(42);
  ASSERT_EQ(false, queue.empty());
  ASSERT_EQ(1, queue.size());

  queue.push(666);
  ASSERT_EQ(false, queue.empty());
  ASSERT_EQ(2, queue.size());

  int value = 0;
  ASSERT_TRUE(queue.pop(value));
  ASSERT_EQ(42, value);
  ASSERT_EQ(false, queue.empty());
  ASSERT_EQ(1, queue.size());

  // Alternative pop
  auto [success, val] = queue.pop();
  ASSERT_TRUE(success);
  ASSERT_EQ(666, val);
  ASSERT_EQ(true, queue.empty());
  ASSERT_EQ(0, queue.size());

  ASSERT_FALSE(queue.pop(value));
}
