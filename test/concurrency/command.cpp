/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/concurrency/command.h>

#include <deque>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <gtest/gtest.h>

namespace lc = liberate::concurrency;


TEST(Command, create_manual)
{
  // This is mostly a compilation test - requires a type in ctor
  lc::command::command_context<int, bool> ctx{42};
  ctx.parameters = std::make_unique<int>(42);
  ctx.results = std::make_unique<bool>(false);
}


TEST(Command, create_convenience_int)
{
  using command = lc::command::command_context<int, bool>;
  auto cmd = lc::command::create_context<command>(10, 42);
  ASSERT_TRUE(cmd);
  ASSERT_EQ(cmd->type, 10);

  auto cast = reinterpret_cast<command *>(cmd.get());

  ASSERT_TRUE(cast->parameters);
  ASSERT_EQ(*(cast->parameters), 42);
}


TEST(Command, create_convenience_complex)
{
  struct params
  {
    int x = 0;
    bool y = true;
  };

  using command = lc::command::command_context<params, bool>;

  auto cmd = lc::command::create_context<command>(10, 42, false);
  ASSERT_TRUE(cmd);
  ASSERT_EQ(cmd->type, 10);

  auto cast = reinterpret_cast<command *>(cmd.get());

  ASSERT_TRUE(cast->parameters);
  ASSERT_EQ(cast->parameters->x, 42);
  ASSERT_EQ(cast->parameters->y, false);
}


TEST(CommandQueueBase, with_deque)
{
  using command = lc::command::command_context<lc::command::void_t, int>;
  auto cmd = std::make_unique<command>(42);

  lc::command::command_queue_base<std::deque> cq;
  cq.enqueue_command(std::move(cmd));

  auto dequeued = cq.dequeue_command();
  ASSERT_TRUE(dequeued);
  ASSERT_EQ(42, dequeued->type);

  ASSERT_FALSE(cq.dequeue_command());

  dequeued->as<command>(42)->results = std::unique_ptr<int>(new int{123});

  cq.put_results(std::move(dequeued));

  auto results = cq.get_completed();
  ASSERT_TRUE(results);

  ASSERT_FALSE(cq.get_completed());
}


TEST(CommandQueueBase, with_concurrent_queue)
{
  using command = lc::command::command_context<>;
  auto cmd = std::make_unique<command>(42);

  lc::command::command_queue_base<liberate::concurrency::concurrent_queue> cq;
  cq.enqueue_command(std::move(cmd));

  auto dequeued = cq.dequeue_command();
  ASSERT_TRUE(dequeued);
  ASSERT_EQ(42, dequeued->type);

  ASSERT_FALSE(cq.dequeue_command());


  cq.put_results(std::move(dequeued));

  auto results = cq.get_completed();
  ASSERT_TRUE(results);

  ASSERT_FALSE(cq.get_completed());
}


TEST(ConcurrentCommandQueue, basic_operations)
{
  lc::command::concurrent_command_queue cq;
  ASSERT_TRUE(cq.empty());
  ASSERT_EQ(0, cq.commands());
  ASSERT_EQ(0, cq.results());
  auto cmd = cq.dequeue_command();
  ASSERT_FALSE(cmd);

  // Command
  using command = lc::command::command_context<lc::command::void_t, bool>;
  cq.enqueue_command<command>(42);
  ASSERT_FALSE(cq.empty());
  ASSERT_EQ(1, cq.commands());
  ASSERT_EQ(0, cq.results());

  cmd = cq.dequeue_command();
  ASSERT_TRUE(cq.empty());
  ASSERT_EQ(0, cq.commands());
  ASSERT_EQ(0, cq.results());
  ASSERT_TRUE(cmd);
  ASSERT_EQ(cmd->type, 42);

  // Results
  cq.put_results<command>(std::move(cmd), true);
  ASSERT_FALSE(cq.empty());
  ASSERT_EQ(0, cq.commands());
  ASSERT_EQ(1, cq.results());

  cmd = cq.get_completed();
  ASSERT_TRUE(cq.empty());
  ASSERT_EQ(0, cq.commands());
  ASSERT_EQ(0, cq.results());
  ASSERT_TRUE(cmd);
  ASSERT_EQ(cmd->type, 42);
}


TEST(ConcurrentCommandQueue, notify_command)
{
  bool notified = false;

  lc::command::concurrent_command_queue cq{[&notified](lc::command::concurrent_command_queue & queue)
    {
      ASSERT_FALSE(queue.empty());
      ASSERT_EQ(1, queue.commands());
      notified = true;
    }
  };
  ASSERT_TRUE(cq.empty());
  ASSERT_EQ(0, cq.commands());
  ASSERT_EQ(0, cq.results());
  auto cmd = cq.dequeue_command();
  ASSERT_FALSE(cmd);

  ASSERT_FALSE(notified);
  using command = lc::command::command_context<>;
  cq.enqueue_command<command>(42);
  ASSERT_TRUE(notified);
}


TEST(ConcurrentCommandQueue, notify_results)
{
  bool notified = false;

  lc::command::concurrent_command_queue cq{{},
    [&notified](lc::command::concurrent_command_queue & queue)
    {
      ASSERT_FALSE(queue.empty());
      ASSERT_EQ(1, queue.results());
      notified = true;
    }
  };
  ASSERT_TRUE(cq.empty());
  ASSERT_EQ(0, cq.commands());
  ASSERT_EQ(0, cq.results());

  ASSERT_FALSE(notified);
  using command = lc::command::command_context<>;
  auto cmd = lc::command::create_context<command>(42);
  cq.put_results(std::move(cmd));
  ASSERT_TRUE(notified);
}



TEST(ConcurrentCommandQueue, notify_both)
{
  size_t notified = 0;
  auto func = [&notified](lc::command::concurrent_command_queue & queue)
  {
    ASSERT_FALSE(queue.empty());
    ++notified;
  };


  lc::command::concurrent_command_queue cq{func, func};

  ASSERT_EQ(0, notified);
  using command = lc::command::command_context<>;
  cq.enqueue_command<command>(42);
  ASSERT_EQ(1, notified);

  auto cmd = cq.dequeue_command();
  cq.put_results(std::move(cmd));
  ASSERT_EQ(2, notified);
}


TEST(ConcurrentCommandQueue, thread_test)
{
  struct ctx
  {
    std::mutex              m = {};
    std::condition_variable cv = {};
    size_t                  notified = 0;
    size_t                  processed = 0;
  };

  ctx c;

  auto notify_func = [&c](lc::command::concurrent_command_queue & queue)
  {
    {
      ASSERT_FALSE(queue.empty());
      std::unique_lock lk{c.m};
      ++c.notified;
    }
    c.cv.notify_one();
  };

  lc::command::concurrent_command_queue cq{notify_func};

  std::thread worker{[&c, &cq]()
    {
      std::unique_lock lk{c.m};
      while (!c.processed) {
        c.cv.wait(lk);

        ASSERT_EQ(1, cq.commands());
        auto cmd = cq.dequeue_command();

        if (cmd) {
          // process...
          ++c.processed;
        }
      }
    }
  };

  // Sleep a little; we don't want to notify the worker before it
  // waits.
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(100ms);

  // Add command
  {
    using command = lc::command::command_context<>;
    cq.enqueue_command<command>(42);
  }

  // Wait for worker
  worker.join();

  ASSERT_EQ(1, c.notified);
  ASSERT_EQ(1, c.processed);
}
