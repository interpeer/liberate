/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/concurrency/lock_policy.h>

#include <gtest/gtest.h>

#include <mutex>

namespace lc = liberate::concurrency;

template <typename T>
class LockPolicy: public ::testing::Test {};
TYPED_TEST_SUITE_P(LockPolicy);

TYPED_TEST_P(LockPolicy, basic_scoped_locking)
{
  using policy = TypeParam;

  // This test doesn't really do anything - but if it works, it not only
  // compiles but exits cleanly.
  typename policy::mutex_type mutex;

  // Lock it successively in two scopes.
  {
    typename policy::lock_type lock{mutex};

    // This line does nothing, but *must* compile. It returns a reference to the
    // lock, for calling member functions -- but not all lock types have member
    // functions.
    lc::lock_ref<policy>(lock);
  }
  {
    typename policy::lock_type lock{mutex};
  }
}


TYPED_TEST_P(LockPolicy, raw_pointer)
{
  using test_policy = liberate::concurrency::lock_policy<
    typename TypeParam::mutex_type *,
    typename TypeParam::lock_type
  >;

  // This test doesn't really do anything - but if it works, it not only
  // compiles but exits cleanly.
  typename TypeParam::mutex_type    mutex;
  typename test_policy::mutex_type  mutex_ptr{&mutex};

  // Lock it successively in two scopes.
  {
    typename test_policy::lock_type lock{mutex_ptr};
  }
  {
    typename test_policy::lock_type lock{mutex_ptr};
  }
}



TYPED_TEST_P(LockPolicy, shared_pointer)
{
  using test_policy = liberate::concurrency::lock_policy<
    std::shared_ptr<typename TypeParam::mutex_type>,
    typename TypeParam::lock_type
  >;

  // This test doesn't really do anything - but if it works, it not only
  // compiles but exits cleanly.
  typename test_policy::mutex_type mutex_ptr = std::make_shared<typename TypeParam::mutex_type>();

  // Lock it successively in two scopes.
  {
    typename test_policy::lock_type lock{mutex_ptr};
  }
  {
    typename test_policy::lock_type lock{mutex_ptr};
  }
}


TYPED_TEST_P(LockPolicy, unique_pointer)
{
  using test_policy = liberate::concurrency::lock_policy<
    std::unique_ptr<typename TypeParam::mutex_type>,
    typename TypeParam::lock_type
  >;

  // This test doesn't really do anything - but if it works, it not only
  // compiles but exits cleanly.
  typename test_policy::mutex_type mutex_ptr{new typename TypeParam::mutex_type{}};

  // Lock it successively in two scopes.
  {
    typename test_policy::lock_type lock{mutex_ptr};
  }
  {
    typename test_policy::lock_type lock{mutex_ptr};
  }
}


TYPED_TEST_P(LockPolicy, reference)
{
  using test_policy = liberate::concurrency::lock_policy<
    typename TypeParam::mutex_type &,
    typename TypeParam::lock_type
  >;

  // This test doesn't really do anything - but if it works, it not only
  // compiles but exits cleanly.
  typename TypeParam::mutex_type mutex;
  typename test_policy::mutex_type mutex_ref{mutex};

  // Lock it successively in two scopes.
  {
    typename test_policy::lock_type lock{mutex_ref};
  }
  {
    typename test_policy::lock_type lock{mutex_ref};
  }
}


REGISTER_TYPED_TEST_SUITE_P(LockPolicy,
    basic_scoped_locking,
    raw_pointer,
    shared_pointer,
    unique_pointer,
    reference
);


typedef ::testing::Types<
  liberate::concurrency::null_lock_policy,
  liberate::concurrency::lock_policy<
    std::mutex, std::lock_guard<std::mutex>
  >,
  liberate::concurrency::lock_policy<
    std::mutex, std::unique_lock<std::mutex>
  >,
  liberate::concurrency::lock_policy<
    std::recursive_mutex, std::lock_guard<std::recursive_mutex>
  >,
  liberate::concurrency::lock_policy<
    std::recursive_mutex, std::unique_lock<std::recursive_mutex>
  >,
  liberate::concurrency::lock_policy<
    std::timed_mutex, std::lock_guard<std::timed_mutex>
  >,
  liberate::concurrency::lock_policy<
    std::timed_mutex, std::unique_lock<std::timed_mutex>
  >,
  liberate::concurrency::lock_policy<
    std::recursive_timed_mutex, std::lock_guard<std::recursive_timed_mutex>
  >,
  liberate::concurrency::lock_policy<
    std::recursive_timed_mutex, std::unique_lock<std::recursive_timed_mutex>
  >
> test_types;
INSTANTIATE_TYPED_TEST_SUITE_P(concurrency, LockPolicy, test_types);


