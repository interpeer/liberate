/**
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <liberate/types/byte.h>

#include <gtest/gtest.h>

TEST(ByteTest, literals)
{
  using namespace liberate::types;
  using namespace liberate::types::literals;

  // Basic conversion
  auto v = 0x42_b;
  ASSERT_EQ(typeid(v), typeid(byte));

  auto s = "hello"_b;
  ASSERT_EQ(typeid(s), typeid(std::vector<byte>));

  auto c = '\x10'_b;
  ASSERT_EQ(typeid(c), typeid(byte));

  // Long integers are truncated
  auto x = 123456_b;
  ASSERT_EQ(typeid(x), typeid(byte));
  ASSERT_EQ((123456 % 256), x);

  // Does not compile (good!)
  // auto y = 1.23_b;
}
