# liberate

<p align="center">
<img src="./docs/liberate.png" />
</p>

[![Build status](https://ci.appveyor.com/api/projects/status/objyc63xyiv2rbf8?svg=true)](https://ci.appveyor.com/project/jfinkhaeuser/liberate)
[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)

liberate is a small platform liberation library for the Interpeer Project.

The scope of the project is bound to the needs of the overall proejct. It may
change over time. For now, the focus is on the subset of platform abstractions
that are shared amongst several other projects:

- [packeteer](https://codeberg.org/interpeer/packeteer)
- [channeler](https://codeberg.org/interpeer/channeler)
- [s3kr1t](https://codeberg.org/interpeer/s3kr1t)
- [caprock](https://codeberg.org/interpeer/caprock)
- [vessel](https://codeberg.org/interpeer/vessel)
- [wyrd](https://codeberg.org/interpeer/wyrd)

**Project Info**

| [ChangeLog](./CHANGES) | [Contributing](./CONTRIBUTING.md) | [Code of Conduct](./CODE_OF_CONDUCT.md) |
|-|-|-|

# 💡 Usage

Liberate is a C++ library targetting C++17; it works as a static and as a
shared library.

```c++
#include <liberate/serialization/integer.h>

uint32_t my_value = 42;

char buf[100];

using namespace liberate::serialization;
auto used = serialize_int(buf, sizeof(buf), my_value);
assert(used == sizeof(my_value));
// => buf now contains 42 in big-endian order in the first four bytes
```

# 📖 API

The API is growing as the needs of the [Interpeer Project](https://interpeer.io)
change. The [full documentation](https://docs.interpeer.io/liberate/) provides
How-Tos and an API reference.

# 🛠️ Installation

If you're using meson, just put this repo (at a version tag of your choice) into
your subprojects as a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules),
e.g.

```bash
$ git submodule add https://codeberg.org/interpeer/liberate subprojects/liberate
$ cd subprojects/liberate
$ git checkout v0.3.0 # or whatever
$ cd ..
$ git commit -m "Added liberate at v0.3.0"
```

When that is done, you can just use liberate in your own `meson.build` file.

```python
# Try system liberate first, fall back to subproject
liberate_dep = dependency(
  'liberate',
  fallback: ['liberate']
)

# Assuming you're building a list of dependencies
deps += [liberate_dep]

summary('liberate', liberate_dep.version(), section: 'Interpeer Dependencies')
```

# ⚖️ License

Liberate is licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html);
[a copy of the license](./LICENSE) is in the repository.

For other licensing options, please contact [Interpeer gUG](https://interpeer.io).

We're a non-profit, however, so if you like this library, please consider
[donating ❤️](https://interpeer.io/donations/). That will make sure the code
stays maintained.
