/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_LIBERATE_H
#define LIBERATE_LIBERATE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <memory>

/**
 * Which platform are we on?
 **/
#if !defined(LIBERATE_PLATFORM_DEFINED)
#  if defined(_WIN32) || defined(__WIN32__) || defined(__WINDOWS__)
#    define LIBERATE_WIN32
#  else
#    define LIBERATE_POSIX
#  endif
#  define LIBERATE_PLATFORM_DEFINED
#endif

// Visibility macros are used by all, so they must come first.
#include <liberate/visibility.h>
#include <liberate/version.h>
#include <liberate/types.h>

/**
 * Decide what to include globally
 **/
#if defined(LIBERATE_WIN32)
// Include windows.h with minimal definitions
#  ifndef WIN32_LEAN_AND_MEAN
#    define WIN32_LEAN_AND_MEAN
#    define __UNDEF_LEAN_AND_MEAN
#  endif
#  define NOMINMAX
// Unicode builds
#  define UNICODE
#  define _UNICODE
#  include <windows.h>
#  include <WinDef.h>
#  ifdef __UNDEF_LEAN_AND_MEAN
#    undef WIN32_LEAN_AND_MEAN
#    undef __UNDEF_LEAN_AND_MEAN
#  endif
#endif


/**
 * All of liberate's definitions live in this namespace. Some select headers
 * provide convenience overloads in the @p std namespace.
 */
namespace liberate {

/**
 * Liberate needs an API instance for some things. This is to avoid global
 * variables in shared objects. If a function manages such "global" state,
 * it will require an API instance parameter.
 */
class LIBERATE_API api
{
public:
  api();
  ~api();

private:
  struct api_impl;
  std::unique_ptr<api_impl> m_impl;
};


} // namespace liberate

#endif // guard
