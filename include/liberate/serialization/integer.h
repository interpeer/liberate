/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_SERIALIZATION_INTEGER_H
#define LIBERATE_SERIALIZATION_INTEGER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <cstddef>
#include <type_traits>

#include <liberate/types/type_traits.h>

namespace liberate::serialization {

/**
 * This struct serves two purposes:
 *
 * 1. The const/volatile overloads help serve to erase the distinction between
 *    the base and qualified types where such a distinction is not necessary.
 * 2. The specializtions for concrete @p T also enable specializations for
 *    specific integer types.
 */
template <typename T>
struct enable_integer_serialization
{};


/** @copydoc enable_integer_serialization */
template <typename T>
struct enable_integer_serialization<T const>
  : enable_integer_serialization<T>
{};

/** @copydoc enable_integer_serialization */
template <typename T>
struct enable_integer_serialization<T volatile>
  : enable_integer_serialization<T>
{};

/** @copydoc enable_integer_serialization */
template <typename T>
struct enable_integer_serialization<T const volatile>
  : enable_integer_serialization<T>
{};


/**
 * This type is defined for all @p T for which @ref enable_integer_serialization
 * is specialized, and all its const/volatile derived types.
 */
template <typename T>
using integer_serialization_enabled = typename enable_integer_serialization<T>::type;

/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<char> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<signed char> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<unsigned char> { using type = std::size_t; /**< @private hide */ };
#if defined(LIBERATE_HAVE_WHCAR_T)
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<wchar_t> { using type = std::size_t; /**< @private hide */ };
#endif
#if defined(LIBERATE_HAVE_CHAR8_T)
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<char8_t> { using type = std::size_t; /**< @private hide */ };
#endif
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<char16_t> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<char32_t> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<short> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<unsigned short> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<int> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<unsigned int> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<long> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<unsigned long> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<long long> { using type = std::size_t; /**< @private hide */ };
/** @copydoc enable_integer_serialization */
template <> struct enable_integer_serialization<unsigned long long> { using type = std::size_t; /**< @private hide */ };

namespace detail {

/**
 * Implementation of the serialization logic for different input and output types.
 */
template <
  typename outT,
  typename inT
>
constexpr integer_serialization_enabled<inT>
serialize_int_impl(outT * output, std::size_t const & output_length, inT const & value)
{
  if (!output || !output_length) {
    return 0;
  }

  constexpr std::size_t const input_size = sizeof(inT);
  constexpr std::size_t const out_unit_size = sizeof(outT);
  if (input_size > out_unit_size * output_length) {
    return 0;
  }

  std::size_t written = 0;
  for (std::size_t i = 0 ; i < input_size / out_unit_size ; ++i) {
    output[i] = static_cast<outT>(value >>
        ((input_size - ((i + 1) * out_unit_size)) * 8)
      );
    ++written;
  }

  return written;
}



/**
 * Implementation of the deserialization logic for different input and output types.
 */
template <
  typename outT,
  typename inT
>
constexpr integer_serialization_enabled<outT>
deserialize_int_impl(outT & output, inT const * input, std::size_t input_length)
{
  if (!input || !input_length) {
    return 0;
  }

  constexpr std::size_t const output_size = sizeof(outT);
  constexpr std::size_t const in_unit_size = sizeof(inT);
  if (output_size > in_unit_size * input_length) {
    return 0;
  }

  std::size_t read = 0;
  output = outT{0};

  using unsigned_inT = typename std::make_unsigned<inT>::type;
  constexpr unsigned_inT const IN_MASK = ~unsigned_inT{0};

  for (std::size_t i = output_size / in_unit_size ; i > 0 ; --i) {
    output += (static_cast<outT>(input[i - 1]) & IN_MASK) << (output_size - (i * in_unit_size)) * 8;
    ++read; // flawfinder: ignore
  }

  return read; // flawfinder: ignore
}


} // namespace detail

/**
 * Serialize to buffer.
 *
 * The buffer is defined by a pointer and a size. The input is an
 * integer value for which serialize_int is explicitly enabled (see above).
 *
 * If the buffer is too small, zero is returned. Otherwise, the number of
 * units written to the buffer is.
 *
 * *Note:* the function is restricted to 8-bit output buffer types only, though
 *       the detail implementation works for other values. The reason is that
 *       for actual cross-platform value serialization, only 8-bit buffers
 *       matter.
 *
 * See also @ref deserialize_int
 *
 * - `outT` The output type, which can be any 8-bit integer type.
 * - `inT` The input type, which can be any of the types enabled by
 *   @ref integer_serialization_enabled.
 *
 * @param [out] output The output buffer to serialize to.
 * @param [in] output_length The size of the output buffer.
 * @param [in] value The value to serialize.
 * @returns The number of bytes of output buffer consumed, or `0` on errors.
 */
template <
  typename outT,
  typename inT,
  std::enable_if_t<liberate::types::is_8bit_type<outT>::value, int> = 0
>
constexpr integer_serialization_enabled<inT>
serialize_int(outT * output, std::size_t output_length, inT const & value)
{
  return detail::serialize_int_impl(output, output_length, value);
}


/**
 * Deserialize from buffer.
 *
 * The buffer is defined by a pointer and a size. The input is an
 * integer value for which deserialize_int is explicitly enabled (see above).
 *
 * If the buffer is too small, zero is returned. Otherwise, the number of
 * units read from the buffer is.
 *
 * *Note:* the function is restricted to 8-bit input buffer types only, though
 *       the detail implementation works for other values. The reason is that
 *       for actual cross-platform value serialization, only 8-bit buffers
 *       matter.
 *
 * See also @ref serialize_int
 *
 * - `outT` The output type, which can be any of the types enabled by
 *   @ref integer_serialization_enabled.
 * - `inT` The input type, which can be any 8-bit integer type.
 *
 * @param [out] output The deserialized value.
 * @param [in] input The input buffer to read from.
 * @param [in] input_length The size of the input buffer.
 * @returns The number of bytes of input buffer consumed, or `0` on errors.
 */
template <
  typename outT,
  typename inT,
  std::enable_if_t<liberate::types::is_8bit_type<inT>::value, int> = 0
>
constexpr integer_serialization_enabled<outT>
deserialize_int(outT & output, inT const * input, std::size_t const & input_length)
{
  return detail::deserialize_int_impl(output, input, input_length);
}

} // namespace liberate::serialization

#endif // guard
