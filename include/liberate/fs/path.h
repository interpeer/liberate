/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_UTIL_PATH_H
#define LIBERATE_UTIL_PATH_H

#include <liberate.h>

namespace liberate::fs {

/**
 * Convert path names from POSIX-like to WIN32.
 *
 * This mostly converts any unquoted backslash unto forward slash and vice
 * versa. But it also converts `<drivename>:\` to `/<drivename>/` and back.
 *
 * In the end, it's this functionality that allows you to use POSIX-style
 * path segments in URLs, and have them map to Windows paths on WIN32.
 *
 * @param [in] other POSIX-style input path.
 * @returns WIN32-style converted path.
 */
LIBERATE_API
std::string to_win32_path(std::string const & other);

/**
 * The inverse of @ref to_win32_path.
 *
 * @param [in] other WIN32-style input path.
 * @returns POSIX-style converted path.
 */
LIBERATE_API
std::string to_posix_path(std::string const & other);

} // namespace liberate::fs

#endif // guard
