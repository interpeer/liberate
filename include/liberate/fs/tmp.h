/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_FS_TMP_H
#define LIBERATE_FS_TMP_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <string>

namespace liberate::fs {

/**
 * Return a file name in a temporary path location.
 *
 * This is useful for e.g. local, pipe or fifo connectors.
 *
 * Note that the function cannot absolutely guarantee uniqeness of the file
 * name returned; consider it a best-effort attempt.
 *
 * In particular, since there is a race between generating the name and
 * opening it for a pipe, this function falls prey to
 * https://cwe.mitre.org/data/definitions/377.html
 *
 * But since mkstemp() and other OS functions cannot themselves create
 * a pipe with a temporary name, this is something we'll have to live with.
 *
 * @param [in] prefix optional; a prefix for the generated file name.
 * @returns temporary file name.
 * @throws std::runtime_error on some failures with system APIs.
 */
LIBERATE_API
std::string temp_name(std::string const & prefix = "");

} // namespace std


#endif // guard
