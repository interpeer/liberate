/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_SYS_ERROR_H
#define LIBERATE_SYS_ERROR_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

namespace liberate::sys {

/**
 * Platform-independent version of getting the last error code, i.e.
 * returns errno on POSIX and WSAGetLastError() on Win32.
 *
 * @returns The last platform error code.
 */
LIBERATE_API
int error_code();


/**
 * Similar, platform-independent version of turning an error code as
 * returned by the above function into an error message. Takes care of
 * UTF-8 encoding, if necessary.
 *
 * @param [in] code A platform error code.
 * @returns a string message describing the error provided by the system.
 */
LIBERATE_API
std::string error_message(int code);


} // namespace liberate::sys

#endif // guard
