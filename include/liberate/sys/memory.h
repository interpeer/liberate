/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_SYS_MEMORY_H
#define LIBERATE_SYS_MEMORY_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

namespace liberate::sys {

/**
 * Platform-independent version of secure memset; ensures memset won't be
 * optimized out. Sets size amount of Bytes in the buffer to the character
 * value given.
 *
 * @param [in,out] buffer The buffer to write to.
 * @param [in] size The size of the buffer.
 * @param [in] value The value to write into each byte of the buffer.
 */
LIBERATE_API void
secure_memset(void * buffer, size_t size, char value);


/**
 * Platform-independent version for secure memory clearing (setting to zero).
 * This may be a bit different than the above function, in that some platforms
 * explicitly provide functions for this purpose. If you want to clear memory,
 * use this function.
 *
 * @param [in,out] buffer The buffer to write to.
 * @param [in] size The size of the buffer.
 */
LIBERATE_API void
secure_memzero(void * buffer, size_t size);

} // namespace liberate::sys

#endif // guard
