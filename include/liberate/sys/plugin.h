/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_SYS_PLUGIN_H
#define LIBERATE_SYS_PLUGIN_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <string>

namespace liberate::sys {

/**
 * Plugins have a user-defined type; it's up to the user of this API to
 * keep different plugin types distinct.
 */
using plugin_type = uint32_t;

/**
 * Plugins may also have different interface versions. Again, it's up the user
 * of this API to define how this field is used precisely.
 */
using plugin_version = uint32_t;

/**
 * A plugin exposes some kind of lookup-table, probably in the form of a
 * data structure with symbols pointing to some function or another. The
 * data structure here is to be defined by the user, hence the void
 * pointer field below.
 *
 * We only define the meta-data for plugins: each plugin shall expose a symbol
 * "liberate_plugin_meta" which points to a function returning a pointer to a
 * structure as defined below.
 */
struct LIBERATE_API plugin_meta
{
  /** The type of this plugin. */
  plugin_type     type;

  /** The version of this plugin. */
  plugin_version  version;

  /** The symbol table or other data. */
  void *          data;

  /** A handle to the plugin itself. */
  void *          plugin_handle;
};


/**
 * Attempt to load plugin metadata from a file.
 *
 * @param [out] meta The metadata structure to fill. Pass a pointer (by
 *    reference), which is set to a non-nullptr value on success.
 * @param [in] filename The name of the file to load.
 *
 * @return True on success, else false.
 */
LIBERATE_API bool
load_plugin(plugin_meta const * & meta, char const * filename);

/**
 * @see load_plugin
 */
LIBERATE_API bool
load_plugin(plugin_meta const * & meta, std::string const & filename);


/**
 * Unload a previously loaded plugin.
 *
 * @param [in, out] The metadata structure representing the loaded
 *    plugin. Will be set to nullptr on success.
 *
 * @return True on success, else false.
 */
LIBERATE_API bool
unload_plugin(plugin_meta const * & meta);


} // namespace liberate::sys

#endif // guard
