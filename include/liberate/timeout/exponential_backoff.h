/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_TIMEOUT_EXPONENTIAL_BACKOFF_H
#define LIBERATE_TIMEOUT_EXPONENTIAL_BACKOFF_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <cmath>

#include <liberate/random/unsafe_bits.h>

namespace liberate::timeout {

/**
 * Implement exponential backoff.
 *
 * Given a number of collisions (failures), return some multiplier for a
 * backoff factor, defined as an integer between 0 and (2^c - 1).
 *
 * @param [in] collisions The number of collisions that occurred so far.
 * @returns the backoff factor.
 */
inline std::size_t
backoff_multiplier(std::size_t const & collisions)
{
  ::liberate::random::unsafe_bits<std::size_t> rng; // flawfinder: ignore
  std::size_t clamp = static_cast<std::size_t>(exp2(collisions)) - 1;
  auto rand = rng.get_factor();
  auto ret = static_cast<std::size_t>(std::nearbyint(rand * clamp));
  return ret;
}


/**
 * The backoff function is statically parametrized with the backoff factor.
 * This is to provide for a simpler function prototype that just requires
 * the number of collisions, and returns the actual backoff value.
 *
 * See also @ref backoff_multiplier
 *
 * @param [in] backoff A factor for backoff times.
 * @param [in] collisions The number of collisions.
 * @param [in] min optional; a minimum backoff value.
 * @returns the calculated backoff based on the arguments.
 */
template <
  typename backoffT
>
inline backoffT
backoff(backoffT const & backoff, std::size_t const & collisions,
    backoffT const & min = {})
{
  return min + (backoff * backoff_multiplier(collisions));
}

} // namespace liberate::timeout


#endif // guard
