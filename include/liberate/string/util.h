/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_STRING_UTIL_H
#define LIBERATE_STRING_UTIL_H

#include <liberate.h>

#include <string>
#include <vector>

namespace liberate::string {

/**
 * Lowercase strings.
 *
 * @param [in] input The string to lowercase
 * @return the lowercased string
 */
LIBERATE_API
std::string to_lower(std::string const & input);

/**
 * Uppercase strings.
 *
 * @param [in] input The string to uppercase
 * @return the uppercased string
 */
LIBERATE_API
std::string to_upper(std::string const & input);


/**
 * Replace needle in haystack with substitute.
 *
 * @param [in] haystack The haystack to replace occurrences in.
 * @param [in] needle The string to find in the haystack.
 * @param [in] substitute The string to substitute for the found needle.
 * @param [in] first_only If set, replace only the first occurrence of the
 *    needle with the substitute. Defaults to false.
 * @return The @p haystack with occurrences of @p needle replaced by
 *    @p substitute.
 */
LIBERATE_API
std::string replace(std::string const & haystack, std::string const & needle,
    std::string const & substitute, bool first_only = false);


/**
 * Perform case-insensitive search.
 *
 * @param [in] haystack The haystack to find a string in.
 * @param [in] needle The needle to find.
 * @return -1 if the needle was not found. Otherwise the position in the string
 *    where the needle was encountered.
 */
LIBERATE_API
ssize_t ifind(std::string const & haystack, std::string const & needle);


/**
 * Split a string by delimiter character(s). Any sequence of delimiters is
 * taken as a single delimiter, i.e. if the second parameter is ";,", a
 * haystack of "foo,;bar" yields two results. There is no third result
 * representing the empty string between the delimiters.
 *
 * @param [in] haystack The haystack to process.
 * @param [in] delimiters The delimiter characters at which to split the string;
 *    each character is treated as an equivalent delimiter to the others. This
 *    defaults to spaces, tabs and newlines.
 * @return a `std::vector` of the split parts.
 */
LIBERATE_API
std::vector<std::string>
split(std::string const & haystack, std::string const & delimiters = " \t\n");


} // namespace liberate::string

#endif // guard
