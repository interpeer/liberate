/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_STRING_URLENCODE_H
#define LIBERATE_STRING_URLENCODE_H

#include <liberate.h>

namespace liberate::string {

/**
 * URL encode.
 *
 * @param [in] input The input to encode.
 * @returns the input with reserved characters percent encoded.
 */
LIBERATE_API
std::string urlencode(std::string const & input);

/**
 * URL decode.
 *
 * @param [in] input The input to decode.
 * @returns the input with reserved characters percent decoded.
 */
LIBERATE_API
std::string urldecode(std::string const & input);

} // namespace liberate::string

#endif // guard
