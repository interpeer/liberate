/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_STRING_UTF8_H
#define LIBERATE_STRING_UTF8_H

#include <liberate.h>

namespace liberate::string {

#if defined(LIBERATE_WIN32)

/**
 * Convert from Windows UCS-2 to UTF-8
 *
 * @param [in] source The input to convert.
 * @returns the input in UTF-8 encoding.
 */
LIBERATE_API
std::string to_utf8(TCHAR const * source);

/**
 * Convert from UTF-8 to Windows UCS-2
 *
 * @param [in] source The input to convert.
 * @returns the input in UCS-2 encoding.
 */
LIBERATE_API
std::wstring from_utf8(char const * source);

#endif

} // namespace liberate::string

#endif // guard
