/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_CONCURRENCY_CONCURRENT_QUEUE_H
#define LIBERATE_CONCURRENCY_CONCURRENT_QUEUE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <atomic>
#include <cstddef>

/**
 * Defines concurrency related functionality.
 */
namespace liberate::concurrency {

/**
 * A simple concurrent queue implementation; lifted with small changes from
 * Herb Sutter's "Writing a Generalized Concurrent Queue"
 * http://drdobbs.com/high-performance-computing/211601363
 *
 * The queue implementation uses a producer spinlock and a consumer spinlock,
 * on the assumption that we'll have multiple producers and consumers.
 * Technically, we only have 1:N and N:1 situations, i.e. one producer and
 * multiple consumers or vice versa. In the interest of simplicity, we'll just
 * stick with the N:M implementation Sutter provides.
 *
 * The main change (other than some symbol name changes) is the addition of
 * the size() and empty() functions, which use the consumer lock and can
 * therefore contend with the consumers.
 *
 * Note that while this implementation uses STL-ish symbol names, it makes no
 * attempt at providing a full STL-like container.
 */
template <typename valueT>
class concurrent_queue
{
public:
  /***************************************************************************
   * STL-ish types
   **/
  using size_type = size_t;   /**< STL-ish usage */
  using value_type = valueT;  /**< STL-ish usage */


  /***************************************************************************
   * Implementation
   **/

  /**
   * Constructor/destructor
   **/
  inline concurrent_queue()
  {
    m_first = m_last = new node(nullptr);
    m_producer_lock = m_consumer_lock = false;
  }



  inline ~concurrent_queue()
  {
    while (m_consumer_lock.exchange(true)) {}
    while (m_producer_lock.exchange(true)) {}

    while (nullptr != m_first) {
      node * tmp = m_first;
      m_first = tmp->m_next;
      delete tmp;
    }
  }



  /**
   * Add new values to the queue with push() and remove them with pop(). The
   * latter returns true if a value could be returned, false otherwise.
   *
   * Multiple producers using push() contend for a producer lock.
   * Multiple consumers using pop() contend for a consumer lock.
   *
   * Producers and consumers do not contend with each other, however.
   *
   * @param [in] value The value to push into the queue.
   **/
  // cppcheck-suppress constParameter
  inline void push(valueT const & value)
  {
    node * tmp = new node(new valueT(value));

    while (m_producer_lock.exchange(true)) {}

    m_last->m_next = tmp;
    m_last = tmp;

    m_producer_lock = false;
  }



  /**
   * Push an entire range into the queue, defined by a beginning and end
   * iterator. Note that the iterator is expected to have a single value, i.e.
   * it is de-referenced with the * operator.
   *
   * @param [in] begin The start of the range.
   * @param [in] end One past the last element of the range, as per standard
   *    C++ usage.
   */
  template <typename iterT>
  inline void push_range(iterT const & begin, iterT const & end)
  {
    for (iterT iter = begin ; iter != end ; ++iter) {
      push(*iter);
    }
  }



  /**
   * Pop a value from the queue.
   *
   * @param [out] result The location where the popped value will be stored.
   * @returns true if there was a value to pop, false otherwise - in which case
   *    the value of @p result is undefined.
   */
  inline bool pop(valueT & result)
  {
    while (m_consumer_lock.exchange(true)) {}

    node * first = m_first;
    node * next = m_first->m_next;

    if (nullptr == next) {
      m_consumer_lock = false;
      return false;
    }

    valueT * val = next->m_value;
    next->m_value = nullptr;
    m_first = next;
    m_consumer_lock = false;

    result = *val;
    delete val;
    delete first;

    return true;
  }


  /**
   * Variant of @ref pop that returns a tuple of values.
   *
   * @returns Tuple containing:
   *  - A boolean flag whether a value was present.
   *  - The retrieved value if the first parameter is true, otherwise a default
   *    constructed value.
   */
  inline std::tuple<bool, valueT>
  pop()
  {
    valueT val{};
    auto res = pop(val);
    return {res, val};
  }


  /**
   * STL-ish information functions on the state of the queue. Both take the
   * consumer's point of view and contend for the consumer lock with pop().
   *
   * Note that empty() is O(1), size() is O(N).
   *
   * It is *not* advisable to use empty() or size() for testing whether or not
   * pop() can be used.
   **/
  inline bool empty() const
  {
    while (m_consumer_lock.exchange(true)) {}

    bool ret = (nullptr == m_first->m_next);

    m_consumer_lock = false;

    return ret;
  }



  /**
   * @returns the current size of the queue (number of entries).
   */
  inline size_type size() const
  {
    while (m_consumer_lock.exchange(true)) {}

    size_type count = 0;
    node * cur = m_first->m_next;
    for ( ; nullptr != cur ; cur = cur->m_next, ++count) {}

    m_consumer_lock = false;

    return count;
  }


private:

  /**
   * Node for the internal linked list.
   **/
  struct node
  {
    explicit node(valueT * value)
      : m_value(value)
      , m_next(nullptr)
    {
    }

    ~node()
    {
      m_next = nullptr;
      delete m_value;
    }

    valueT *            m_value;
    std::atomic<node *> m_next;
  };

  node *                    m_first;
  mutable std::atomic<bool> m_consumer_lock;

  node *                    m_last;
  mutable std::atomic<bool> m_producer_lock;
};

} // namespace liberate::concurrency

#endif // guard
