/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_CONCURRENCY_LOCK_POLICY_H
#define LIBERATE_CONCURRENCY_LOCK_POLICY_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <memory>

namespace liberate::concurrency {

/**
 * See null_lock_policy below; this is the detail namespace.
 */
namespace detail {

/**
 * The null mutex type does nothing.
 */
struct null_mutex
{
};


/**
 * The null lock type takes a mutex, but does nothing with it.
 */
template <typename mutexT>
struct null_lock
{
  template <typename discardT>
  inline null_lock(discardT &)
  {
  }
};


/**
 * Helper for is_smart_ptr
 */
template<class T, class R = void>
struct enable_if_smart_ptr_type { typedef R type; };

/**
 * is_smart_ptr checks for the presence of T::element_type, and assumes that
 * if it's present, it's a smart pointer.
 */
template<class T, class Enable = void>
struct is_smart_ptr : std::false_type {};

template<class T>
struct is_smart_ptr<
  T,
  typename enable_if_smart_ptr_type<
    typename T::element_type
  >::type
> : std::true_type
{};


/**
 * The mutex_ptr_lock_proxy takes a pointer to a mutex - raw or smart, it does
 * not matter - and passes it on to an internal lock instance. In this way, it
 * does whaterver its lockT would when passed a mutex reference.
 */
template <typename mutex_ptrT, typename lockT>
struct mutex_ptr_lock_proxy
{
  template <typename... argsT>
  inline mutex_ptr_lock_proxy( mutex_ptrT & mutex_ptr, argsT &&... args)
    : m_lock{*mutex_ptr, std::forward<argsT>(args)...}
  {
  }

  static inline lockT & lock_ref(mutex_ptr_lock_proxy & proxy)
  {
    return proxy.m_lock;
  }

  lockT m_lock;
};


/**
 * Select whether to use a regular lock (regular mutex) or a lock proxy
 * (pointer to mutex).
 */
template <
  typename mutexT,
  typename lockT,
  bool IS_POINTER,
  bool IS_SMART_POINTER
>
struct lock_selector
{
  // Also selected for IS_POINTER && IS_SMART_POINTER, which should never
  // happen, so it's good that it fails.
};

/**
 * @copydoc lock_selector
 *
 * Raw mutex or references; use lockT as-is.
 */
template <
  typename mutexT,
  typename lockT
>
struct lock_selector<mutexT, lockT, false, false>
{
  using mutex_type = mutexT;
  using lock_type = lockT;

  static inline lockT & lock_ref(lockT & lock)
  {
    return lock;
  }
};

/**
 * @copydoc lock_selector
 *
 * Raw mutex pointer; use lock_proxy
 */
template <
  typename mutexT,
  typename lockT
>
struct lock_selector<mutexT, lockT, true, false>
{
  using mutex_type = mutexT;
  using lock_type = mutex_ptr_lock_proxy<
    mutexT, lockT
  >;

  static inline lockT & lock_ref(lockT & lock)
  {
    return lock_type::lock_ref(lock);
  }
};

/**
 * @copydoc lock_selector
 *
 * Smart mutex pointer; use lock_proxy
 */
template <
  typename mutexT,
  typename lockT
>
struct lock_selector<mutexT, lockT, false, true>
{
  using mutex_type = mutexT;
  using lock_type = mutex_ptr_lock_proxy<
    mutexT, lockT
  >;

  static inline lockT & lock_ref(lockT & lock)
  {
    return lock_type::lock_ref(lock);
  }
};


} // namespace detail


/**
 * Do you want to write code that may or may not require concurrent access
 * and need lock based synchronization? Lock policies are for you.
 *
 * Are you not sure if your code should own/hold a lock, or you need a shared
 * lock? The shared_lock_policy can be combined with other lock policies to
 * meet your needs.
 *
 * A lock policy is a simple struct that defines a mutex_type and a lock_type.
 * The only requirement on the mutex_type is that it defines a lock() and an
 * unlock() function. The only requirement on the lock_type is that it takes
 * a mutex_type reference in the constructor and calls its lock() function, and
 * in the destructor calls it's unlock() function.
 *
 * This follows the basic pattern of scoped locks as you might know them from
 * boost and modern C++. No other patterns are supported to keep things simple,
 * such as timed locks or temporarily unlocking, etc.
 *
 * - The null_lock_policy defines types that do nothing. Use this when you want
 *   to specialize your code to not perform synchronization.
 *
 * - The @ref lock_policy type also defines a mutex_type and a
 *   lock_type, but in doing so resolves whether mutexT is a raw or smart
 *   pointer.
 *   - If mutexT is a mutex type, lock_type becomes lockT.
 *   - If mutexT is a raw or smart pointer to a mutex type, lock_type becomes
 *     a proxy that dereferences its argument before passing it on to an internal
 *     lockT instance.
 */
struct null_lock_policy
{
  /** mutex type for the policy */
  using mutex_type = detail::null_mutex;
  /** lock type for the policy */
  using lock_type = detail::null_lock<mutex_type>;

  /** @private hide */
  using _passed_lock_type = lock_type;
};


/**
 * @copydoc null_lock_policy
 *
 * Resolves whether mutexT is a raw or smart pointer, and uses a proxy if so.
 * Otherwise uses a the regular lockT.
 */
template <typename mutexT, typename lockT>
struct lock_policy
{
  /** @private hide */
  using _selector = detail::lock_selector<
    mutexT,
    lockT,
    std::is_pointer<mutexT>::value,
    detail::is_smart_ptr<mutexT>::value
  >;

  /** mutex type for the policy */
  using mutex_type = typename _selector::mutex_type;

  /** lock type for the policy */
  using lock_type = typename _selector::lock_type;

  /** @private hide */
  using _passed_lock_type = lockT;
};


/**
 * Dereference a lock (raw or proxied) so its member functions can be accessed.
 * @param [in] lock Raw lock or proxied lock.
 * @returns A reference to the raw lock.
 */
template <typename lock_policyT>
inline typename lock_policyT::_passed_lock_type &
lock_ref(typename lock_policyT::lock_type & lock)
{
  return lock_policyT::_selector::lock_ref(lock);
}

/** @private specialization */
template <>
inline typename null_lock_policy::_passed_lock_type &
lock_ref<null_lock_policy>(typename null_lock_policy::lock_type & lock)
{
  return lock;
}


} // namespace liberate::concurrency

#endif // guard
