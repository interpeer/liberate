/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_CONCURRENCY_COMMAND_H
#define LIBERATE_CONCURRENCY_COMMAND_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <memory>
#include <functional>

#include <liberate/concurrency/concurrent_queue.h>

/**
 * The classes in this namespace implement a generic command queue. The goal,
 * as one might expect, is to provide an asynchronous call mechanism.
 */
namespace liberate::concurrency::command {

/**
 * Commands are identified by an integer command type; the only requirement is
 * that command types are uniquely mapped to commands (and vice versa).
 *
 * A context transports parameters and (eventually) results between both ends
 * of the queue.
 */
using command_type = int;

/**
 * The commmand_context_base is a simple base type for @c command_context,
 * which largely exists for providing a virtual destructor so that derived
 * types destruct properly.
 *
 * Concrete command contexts are specializations of a template based on the
 * command parameters, which means providing a different means for a virtual
 * base type is not possible.
 */
struct command_context_base
{
  /**
   * Constructor for the context; takes a command type.
   *
   * @param [in] _type the command type for this context.
   */
  inline explicit command_context_base(command_type _type)
    : type{_type}
  {
  }

  /** Virtual destructor */
  virtual ~command_context_base() = default;

  /**
   * Cast this command_context_base into the given type if the argument matches
   * the enclosed type.
   *
   * @param [in] target_type The type we expect.
   * @return a raw pointer to the derived type, or nullptr on mismatch.
   */
  template<
    typename derivedT
  >
  inline derivedT *
  as(command_type const & target_type)
  {
    if (target_type != type) {
      return nullptr;
    }
    return reinterpret_cast<derivedT *>(this);
  }

  /** @copydoc as */
  template<
    typename derivedT
  >
  inline derivedT const *
  as(command_type const & target_type) const
  {
    if (target_type != type) {
      return nullptr;
    }
    return reinterpret_cast<derivedT const *>(this);
  }


  //! The context's type is a publicly accessible member.
  command_type const  type;
  //! Command contexts can also hold context state; this pointer is for that
  //! purpose.
  void *              context = nullptr;
};


/**
 * Empty structure for when either parameters or results are not required.
 */
struct void_t {};


/**
 * The actual command contexts expands upon the base by providing a parameter
 * and result type. Note that in order to pass multiple parameters or results,
 * you can use a struct or tuple, etc. here.
 *
 * @param [in] paramsT The type of parameter the command context can hold.
 * @param [in] resultsT The result type the command context can hold.
 */
template <
  typename paramsT = void_t,
  typename resultsT = void_t
>
struct command_context : public command_context_base
{
  //! Alias for template parameter @p paramsT
  using params_type = paramsT;
  //! Alias for template parameter @p resultsT
  using results_type = resultsT;

  /**
   * Like its ancestor, the command_context is initialized with a command type.
   *
   * @param [in] _type the command type for this context.
   */
  inline explicit command_context(command_type _type)
    : command_context_base{_type}
  {
  }

  /** Virtual destructor */
  virtual ~command_context() = default;

  //! Commands may hold parameters.
  std::unique_ptr<params_type>  parameters = {};
  //! Commands may also hold a result.
  std::unique_ptr<results_type> results = {};
};


/**
 * A convenience function for creating the command context passes variadic
 * arguments on to the parameters constructor.
 *
 * The @p commandT template parameter is expected to:
 *
 * 1. Be constructible with the @p type parameter below.
 * 2. To contain a @p params_type inner type, which can be constructed by
 *    forwarding the remaining arguments.
 *
 * @param [in] type The command type to create a context for.
 * @param [in] args Variable number of arguments passed to the command.
 * @return a unique_ptr<> of a context for the given command and arguments.
 */
template <
  typename commandT,
  typename... argsT
>
inline std::unique_ptr<command_context_base>
create_context(command_type type, argsT && ...args)
{
  auto ptr = std::make_unique<commandT>(type);
  ptr->parameters = std::unique_ptr<
    typename commandT::params_type
  >{new typename commandT::params_type{std::forward<argsT>(args)...}};
  return ptr;
}


/**
 * The command queue base functionality is provided separate from the more
 * convenient interface, so we can provide easier adaptation to different queue
 * clases, e.g. std::list or liberate's own concurrent_queue.
 *
 * The base implementation implements an STL-like container with push_back() and
 * pop_front() as deque would provide.
 */
template <
  template <typename...> typename queueT
>
class command_queue_base
{
public:
  //! Alias for a unique_ptr of @ref command_context_base, the main item
  //! managed in the queue.
  using command_ptr = std::unique_ptr<command_context_base>;

  /** Virtual destructor */
  virtual ~command_queue_base() = default;

  /**
   * Enqueue a command. This takes ownership of the @p command parameter.
   *
   * @param [in,out] command A command such as created with e.g. @ref create_context.
   */
  inline void enqueue_command(command_ptr && command)
  {
    push(to_process, std::move(command));
  }

  /**
   * Dequeue a command.
   *
   * @return A @ref command_ptr, which may be @p nullptr if there is no command
   *   in the queue.
   */
  inline command_ptr dequeue_command()
  {
    return pop(to_process);
  }


  /**
   * Push results into the response queue. This also accepts a @ref command_ptr,
   * and also takes ownership, just like @ref enqueue_command - but it is used for
   * when command processing completed.
   *
   * @param [in,out] results A completed command (with results).
   */
  inline void put_results(command_ptr && results)
  {
    push(done, std::move(results));
  }


  /**
   * Retrieve a completed command (with results), or @p nullptr if none is in
   *  the queue.
   *
   * @return The @ref command_ptr with results (or @p nullptr).
   */
  inline command_ptr get_completed()
  {
    return pop(done);
  }


private:

  using queue_impl = queueT<command_ptr>;
  queue_impl  to_process;
  queue_impl  done;

  inline static void push(queue_impl & queue, command_ptr && value)
  {
    queue.push_back(std::move(value));
  }

  inline static command_ptr pop(queue_impl & queue)
  {
    if (queue.empty()) {
      return {};
    }
    auto res = std::move(queue.front());
    queue.pop_front();
    return res;
  }

};


/**
 * Specialization of @c command_queue_base for
 * @c liberate::concurrency::concurrent_queue.
 */
template <>
class command_queue_base<::liberate::concurrency::concurrent_queue>
{
public:
  /** @copydoc command_queue_base::command_ptr */
  using command_ptr = std::shared_ptr<command_context_base>;

  /** @copydoc command_queue_base::~command_queue_base */
  virtual ~command_queue_base() = default;

  /** @copydoc command_queue_base::enqueue_command */
  inline void enqueue_command(command_ptr && command)
  {
    push(to_process, std::move(command));
  }

  /** @copydoc command_queue_base::dequeue_command */
  inline command_ptr dequeue_command()
  {
    return pop(to_process);
  }

  /** @copydoc command_queue_base::put_results */
  inline void put_results(command_ptr && results)
  {
    push(done, std::move(results));
  }

  /** @copydoc command_queue_base::get_completed */
  inline command_ptr get_completed()
  {
    return pop(done);
  }

private:
  using queue_impl = typename ::liberate::concurrency::concurrent_queue<command_ptr>;
  queue_impl  to_process = {};
  queue_impl  done = {};


  inline static void push(queue_impl & queue, command_ptr && command)
  {
    queue.push(std::move(command));
  }

  inline static command_ptr pop(queue_impl & queue)
  {
    command_ptr res;
    if (queue.pop(res)) {
      return res;
    }
    return {};
  }
};


/**
 * With the base in place, we can offer more convenient command queue functions.
 */
template <
  template <typename> typename queueT
>
class parametrized_command_queue : private command_queue_base<queueT>
{
public:
  //! Convenience alias for base queue type.
  using base_type = command_queue_base<queueT>;
  //! Convenience alias for command_ptr
  using command_ptr = typename base_type::command_ptr;

  /**
   * A notification function can be invoked when there are items in the command
   * queue.
   */
  using notification_function = std::function<void (parametrized_command_queue &)>;

  /**
   * Constructors take zero, one or two notification functions. If two are given,
   * the first is invoked in-line whenever a command is enqueued. The escond is
   * given whenever a result is enqueued.
   *
   * If only one is given, the function is invoked for commands and results.
   *
   * If none is given, there is no notification of added content.
   */
  inline parametrized_command_queue() = default;

  /**
   * @copydoc parametrized_command_queue
   *
   * @param [in] general_notification A @ref notification_function that is invoked
   *    both when commands or results are placed in the queue.
   */
  inline explicit parametrized_command_queue(
      notification_function general_notification
  )
    : m_command_func{general_notification}
    , m_result_func{general_notification}
  {
  }

  /**
   * @copydoc parametrized_command_queue
   *
   * @param [in] command_notification A @ref notification_function that is invoked
   *    when commands are placed in the queue.
   * @param [in] result_notification A @ref notification_function that is invoked
   *    when results are placed in the queue.
   */
  inline explicit parametrized_command_queue(
      notification_function command_notification,
      notification_function result_notification
  )
    : m_command_func{command_notification}
    , m_result_func{result_notification}
  {
  }

  /** Virtual destructor */
  virtual ~parametrized_command_queue() = default;


  /**
   * Convenience variant of @ref enqueue_command that creates the command
   * context with @ref create_context before enqueueing it.
   */
  template <
    typename commandT,
    typename... argsT
  >
  inline void
  enqueue_command(command_type type, argsT && ...args)
  {
    auto ptr = create_context<commandT>(type, std::forward<argsT>(args)...);
    enqueue_command(std::move(ptr));
  }

  /**
   * @copydoc command_queue_base::enqueue_command
   *
   * Invokes the command notification function.
   */
  inline void enqueue_command(command_ptr && command)
  {
    base_type::enqueue_command(std::move(command));
    ++m_command_size;
    if (m_command_func) {
      m_command_func(*this);
    }
  }


  /** @copydoc command_queue_base::dequeue_command */
  inline command_ptr dequeue_command()
  {
    auto ret = std::move(base_type::dequeue_command());
    if (ret) {
      --m_command_size;
    }
    return ret;
  }


  /**
   * @copydoc command_queue_base::put_results
   *
   * Invokes the result notification function.
   */
  inline void put_results(command_ptr && results)
  {
    base_type::put_results(std::move(results));
    ++m_result_size;
    if (m_result_func) {
      m_result_func(*this);
    }
  }

  template <
    typename commandT,
    typename... argsT
  >
  inline void put_results(command_ptr && results, argsT && ...args)
  {
    if (results) {
      // There can be no error here as we're passing the type we're going to
      // compare to.
      auto derived = results->template as<commandT>(results->type);
      auto ptr = std::unique_ptr<typename commandT::results_type>(
        new typename commandT::results_type{std::forward<argsT>(args)...}
      );
      derived->results = std::move(ptr);
    }
    put_results(std::move(results));
  }


  /** @copydoc command_queue_base::get_completed */
  inline command_ptr get_completed()
  {
    auto ret = std::move(base_type::get_completed());
    if (ret) {
      --m_result_size;
    }
    return ret;
  }


  /**
   * @return the number of commands in queue.
   */
  inline std::size_t commands() const
  {
    return m_command_size;
  }

  /**
   * @return the number of results in queue.
   */
  inline std::size_t results() const
  {
    return m_result_size;
  }

  /**
   * @return true if there are no commands or results in queue, false if either
   * exist.
   */
  inline bool empty() const
  {
    return m_command_size == 0 && m_result_size == 0;
  }

private:
  std::atomic<std::size_t>  m_command_size = 0;
  std::atomic<std::size_t>  m_result_size  = 0;

  notification_function     m_command_func = {};
  notification_function     m_result_func = {};
};

/**
 * The default is a concurrent version.
 */
using concurrent_command_queue = parametrized_command_queue<
  ::liberate::concurrency::concurrent_queue
>;

} // namespace liberate::concurrency::command


#endif // guard
