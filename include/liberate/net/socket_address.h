/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_NET_SOCKET_ADDRESS_H
#define LIBERATE_NET_SOCKET_ADDRESS_H

// *** Config
#include <liberate.h>

// *** C++ includes
#include <iostream>
#include <string>
#include <stdexcept>
#include <cstddef>
#include <memory>

// *** Own includes
#include <liberate/cpp/operators/comparison.h>
#include <liberate/net/address_type.h>
#include <liberate/types/byte.h>

namespace liberate::net {

/*****************************************************************************
 * Forward declarations
 **/
class network;



/*****************************************************************************
 * Socket address
 **/
/**
 * Make it possible to use `struct sockaddr` as a map key.
 **/
class LIBERATE_API socket_address
  : public ::liberate::cpp::comparison_operators<socket_address>
{
public:

  /**
   * Default constructor. The resulting socket address does not point anywhere.
   **/
  socket_address();

  /**
   * Move semantics should remain intact; copies and assignment on the other
   * hand need to create a new, distinct address.
   */
  inline socket_address(socket_address && other) = default;

  /** Copy constructor */
  socket_address(socket_address const & other);

  /** Assignment operator */
  socket_address & operator=(socket_address const & other);

  /** Virtual destructor **/
  virtual ~socket_address() = default;

  /**
   * Constructor. The 'buf' parameter is expected to be a `struct sockaddr` of
   * the given length.
   *
   * @param [in] buf A POSIX `struct sockaddr`.
   * @param [in] len The size of the @p buf parameter.
   **/
  socket_address(void const * buf, size_t len);

  /**
   * Construct from address type and buffer. This constructor selects the type
   * of `struct sockaddr` used internally.
   *
   * @param [in] type The type of address to construct.
   * @param [in] buf A POSIX `struct sockaddr`.
   * @param [in] len The size of the @p buf parameter.
   * @param [in] port optional; defaults to `0`.
   **/
  socket_address(address_type type, void const * buf, size_t len,
      uint16_t port = 0);


  /**
   * Alternative constructor. The string is expected to be a network address
   * in CIDR notation (without the netmask).
   *
   * Results in an address of @ref type @ref AT_UNSPEC if parsing
   * fails.
   *
   * @param [in] address The CIDR address to parse and assign to this
   *  socket_address.
   * @param [in] port optional; defaults to `0`.
   **/
  explicit socket_address(std::string const & address, uint16_t port = 0);

  /**
   * @copydoc socket_address(std::string const & address, uint16_t port)
   *
   * @param [in] size The length of the @p address.
   */
  explicit socket_address(char const * address, uint16_t port = 0, size_t size = 0);


  /**
   * Verifies the given address string would create a valid IP socket address.
   *
   * @param [in] address Address string in CIDR notation.
   * @returns true if the @p address could be parsed, false otherwise.
   **/
  static bool verify_cidr(std::string const & address);


  /**
   * Verifies that the given netmask would work for the given socket address.
   * This really tests whether the socket address is an IPv4 or IPv6 address,
   * and whether the netmask fits that.
   *
   * @param [in] netmask The network mask to test against the address.
   * @returns true if the netmask fits the address type, false otherwise.
   **/
  bool verify_netmask(size_t const & netmask) const;

  /**
   * @returns the maximum possible network mask for this address type.
   */
  size_t max_netmask() const;


  /**
   * Return a CIDR-style string representation of this address (minus port).
   * Only applicable to IP addresses.
   *
   * @returns a CIDR-style address or an empty string.
   **/
  std::string cidr_str() const;


  /**
   * Only applicable to IP addresses.
   *
   * @returns the port part of this address.
   **/
  uint16_t port() const;


  /**
   * @returns the socket's @ref address_type
   **/
  address_type type() const;


  /**
   * @returns a full string representation of the address, including port.
   **/
  std::string full_str() const;


  /**
   * @returns the type specific, used size of the raw address buffer.
   **/
  size_t bufsize() const;


  /**
   * @returns the available size of the raw address buffer.
   **/
  inline static constexpr size_t bufsize_available()
  {
    // See https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/ms740504(v=vs.85)
    // Other socket address types, e.g. sa_un are defined to be slightly
    // smaller; this should always be the maximum size in actual use, plus
    // some optional padding.
    return 128;
  }


  /** @returns the raw address buffer, i.e. a pointer to a `struct sockaddr`.  **/
  void const * buffer() const;
  /** @copydoc buffer */
  void * buffer();


  /**
   * Returns the minimum buffer size required to serialize the address with
   * or without type and port. For IPv4 this is 4 Bytes, for example, without a
   * port or type.
   * *Note*: This is only defined for IPv4 and IPv6 addresses.
   *
   * @param [in] with_type If provided, adds an extra byte to encode the
   *  address type.
   * @param [in] with_port If provided, adds two bytes for a port.
   * @returns The calculated buffer size.
   */
  size_t min_bufsize(bool with_type = true, bool with_port = true) const;


  /**
   * Serialize addresses to mininum buffer. Returns the number of bytes
   * serialized.
   *
   * This function is useful for writing to IP headers.
   *
   * See also @ref deserialize.
   *
   * *Note*: This is only defined for IPv4 and IPv6 addresses.
   *
   * @param [out] buf The output buffer to write to.
   * @param [in] len The length of the output buffer.
   * @param [in] with_type Whether the @ref address_type should also
   *  be encoded.
   * @param [in] with_port Whether the port should also be encoded.
   * @returns the amount of @p buf consumed, or `0` on errors.
   */
  size_t serialize(void * buf, size_t len, bool with_type = true, bool with_port = true) const;


  /**
   * Reconstruct address from minimal buffer size. If a type is provided in the
   * call, it is assumed that the buffer does *not* contain a type. Otherwise,
   * the type is expected in the first Byte. Be aware of this when deserializing
   * addresses.
   *
   * This function is useful for reading from IP headers.
   *
   * The first result is the number of Bytes consumed, the second the result. If
   * the number of Bytes is zero, this indicates an error in parsing.
   *
   * See also @ref serialize.
   *
   * *Note*: This is only defined for IPv4 and IPv6 addresses.
   *
   * @param [in] buf The input buffer to parse.
   * @param [in] len The length of the input buffer.
   * @param [in] with_port Set if the function should also parse a port.
   * @returns Tuple containing:
   *  - The amount of input buffer consumed (`0` on errors)
   *  - The resulting socket_address.
   */
  static std::tuple<size_t, socket_address>
  deserialize(void const * buf, size_t len, bool with_port = true);

  /**
   * @copydoc deserialize
   * @param [in] type Specify the address type to look for; this is only
   *  defined for @ref AT_INET4 and @ref AT_INET6 addresses. Anything else
   *  or parse errors results in an empty result.
   */
  static std::tuple<size_t, socket_address>
  deserialize(address_type type, void const * buf, size_t len,
      bool with_port = true);


  /**
   * Sets/overwrites the port used for this socket address. Returns
   * Returns ERR_INVALID_OPTION if used on the wrong (AT_LOCAL) socket
   * address type.
   **/
  bool set_port(uint16_t port);


  /**
   * @returns true if this is an IP "ANY" address (e.g. INADDR_ANY), false if
   *  not - or if the address type does not contain such a definition.
   */
  bool is_any() const;

  /**
   * @returns true if this is an IP loopback address, false if not - or if the
   *  address type does not contain such a definition.
   */
  bool is_loopback() const;


  /**
   * Swap values of two socket addresses.
   *
   * @param [in,out] other The address to swap values with.
   **/
  void swap(socket_address & other);

  /**
   * @returns a hash for this address, to be used in unordered maps.
   */
  size_t hash() const;


  /**
   * Increment. Returns the address + 1, e.g. 192.168.0.2 if the address is
   * 192.168.0.1.
   *
   * This function does not care about overflows.
   **/
  void operator++();

  /** Used by cpp::comparison_operators */
  bool is_equal_to(socket_address const & other) const;
  /** Used by cpp::comparison_operators */
  bool is_less_than(socket_address const & other) const;


private:
  struct socket_address_impl;
  std::shared_ptr<socket_address_impl>  m_impl;

  friend LIBERATE_API_FRIEND std::ostream & operator<<(std::ostream & os, socket_address const & addr);
  friend class LIBERATE_API_FRIEND network;
};


/**
 * Formats a socket_address into human-readable form.
 **/
LIBERATE_API std::ostream & operator<<(std::ostream & os, socket_address const & addr);

/**
 * Swappable
 **/
inline void
swap(socket_address & first, socket_address & second)
{
  return first.swap(second);
}

} // namespace liberate::net


/*******************************************************************************
 * std namespace specializations
 **/
namespace std {

/**
 * Specialization of `std::hash` for @ref liberate::net::socket_address
 */
template <> struct LIBERATE_API hash<liberate::net::socket_address>
{
  /** Same as `std::hash` */
  size_t operator()(liberate::net::socket_address const & x) const
  {
    return x.hash();
  }
};


} // namespace std


#endif // guard
