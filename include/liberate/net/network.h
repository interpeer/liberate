/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_NET_NETWORK_H
#define LIBERATE_NET_NETWORK_H

// *** Config
#include <liberate.h>

// *** C++ includes
#include <string>
#include <memory>
#include <stdexcept>

// *** Own includes
#include <liberate/cpp/operators/comparison.h>
#include <liberate/net/address_type.h>
#include <liberate/net/socket_address.h>

namespace liberate::net {

/*****************************************************************************
 * Forward declarations
 **/
class socket_address;


/*****************************************************************************
 * Network
 **/
/**
 * Offers operations on networks, including allocation of available addresses
 * within a network.
 **/
class LIBERATE_API network
  : public ::liberate::cpp::comparison_operators<network>
{
public:
  /**
   * Constructor. The netspec is expected to be an IP network specification in
   * CIDR notation. The constructor will throw an exception if the netspec
   * cannot be parsed.
   *
   * Note that due to memory limitations, it is all but impossible to manage
   * all possible IPv6 in a large network. As such, we artificially limit the
   * number of addresses you can manage with this class to 2^64.
   *
   * @param [in] netspec A network specification in CIDR notation.
   * @throws std::invalid_argument If the specification could not be parsed.
   * @throws std::domain_error If the network mask does not match the address
   *    type.
   **/
  explicit network(std::string const & netspec);

  /** Virtual destructor */
  virtual ~network();

  /**
   * The copy constructor creates a *new* network with none of the state used
   * to manage reserved addresses. It is functionally equivalent to writing
   *
   * @code{.cpp}
   *   network n1{spec};
   *   network n2{n1.netspec()};
   * @endcode
   *
   * The same applies to the assignment operator.
   */
  network(network const & other);

  /** Standard assignment operator. */
  network & operator=(network const & other);


  /**
   * Throws away all state in the network class and reinitializes it with the
   * given new netspec.
   *
   * @param [in] netspec A network specification in CIDR notation.
   **/
  void reset(std::string const & netspec);


  /**
   * Verifies the given netspec string would create a valid network.
   *
   * @param [in] netspec A network specification in CIDR notation.
   * @returns true if the given netspec can be parsed and verified, otherwise
   *    false
   **/
  static bool verify_netspec(std::string const & netspec);


  /**
   * @returns the network mask size in bits.
   **/
  size_t mask_size() const;


  /**
   * @returns the maximum amount of allocatable addresses in the network.
   **/
  uint64_t max_size() const;


  /**
   * Returns the network family as a socket_address_type. This is one of
   * AT_INET4 or AT_INET6. May return AT_UNSPEC for badly constructed
   * networks.
   *
   * @returns address_type of the network.
   **/
  address_type family() const;


  /**
   * @returns true if the given address is part of the network, false otherwise.
   **/
  bool in_network(socket_address const & address) const;


  /**
   * @returns the local, network, default gateway and broadcast addresses of this
   * network respectively.
   **/
  socket_address local_address() const;
  /** @copydoc local_address */
  socket_address network_address() const;
  /** @copydoc local_address */
  socket_address gateway_address() const;
  /** @copydoc local_address */
  socket_address broadcast_address() const;

  /**
   * @returns a netspec that can be used to create an equivalent network
   * instance.
   */
  std::string netspec() const;


  /**
   * This functions helps in allocating individual addresses in a network, by
   * keeping internal state on which addresses already have been allocated.
   *
   * It returns a new @ref socket_address (with port set to 0) that is part of
   * this network, or an empty address if there are no available addresses.
   *
   * @returns the allocated address.
   * @throws std::out_of_range If too many addresses were reserved already.
   **/
  socket_address reserve_address();

  /**
   * Return a socket address for an identifier. Note that this does *not*
   * reserve this address, it just performs the mapping. Use
   * @ref reserve_address to actually reserve the address, or @ref is_reserved
   * to query whether it is already reserved.
   *
   * Note that the mapping does *not* keep internal state. Instead, the
   * identifier is hashed, and a choice of address is made based on this hash.
   * The hash in use is not cryptographically secure and may yield collisions.
   *
   * Providing the same identifier string will always yield the same address.
   * Two or more identifier strings may yield the same address. The chances of
   * collisions are much lower when larger networks are used.
   *
   * @param [in] identifier An identifier for an address.
   * @returns A socket address for an identifier.
   */
  socket_address mapped_address(std::string const & identifier) const;

  /**
   * @copydoc mapped_address
   *
   * @param [in] length The length of the identifier.
   */
  socket_address mapped_address(void const * identifier, size_t const & length);

  /**
   * This function acts like @ref reserve_address and @ref mapped_address
   * combined.
   *
   * @param [in] identifier An identifier for an address.
   * @returns the allocated address.
   * @throws std::out_of_range If a hash collision occurred, i.e. two
   *    identifiers mapped to the same address.
   **/
  socket_address reserve_address(std::string const & identifier);

  /**
   * @copydoc reserve_address(std::string const & identifier)
   *
   * @param [in] length The length of the identifier.
   **/
  socket_address reserve_address(void const * identifier, size_t const & length);

  /**
   * Finally, allow reserving an address directly. We don't return the address
   * here, just a boolean flag to determine whether it was successful.
   *
   * @param [in] addr The address to reserve.
   * @returns true if reservation was successful, false otherwise.
   **/
  bool reserve_address(socket_address const & addr);

  /**
   * Releases an address that is part of this network back into the pool. If
   * the address is not in the network, false is returned. Otherwise, true is
   * returned, and the next call to reserve_address() could return the same
   * address again.
   *
   * @param [in] addr The address to release.
   * @returns true if release was successful, false otherwise.
   **/
  bool release_address(socket_address const & addr);

  /**
   * @returns true if an address is reserved already with the given identifier,
   * false otherwise.
   *
   * @param [in] identifier The identifier to check.
   */
  bool is_reserved(std::string const & identifier) const;

  /**
   * @copydoc is_reserved
   *
   * @param [in] length The length of the identifier.
   * @throws std::invalid_argument if the identifier or length are not provided.
   */
  bool is_reserved(void const * identifier, size_t const & length) const;

  /**
   * @returns true if the given address is reserved, false otherwise.
   */
  bool is_reserved(socket_address const & addr) const;

protected:
  /** Used by cpp::comparison_operators */
  friend struct liberate::cpp::comparison_operators<network>;

  /** Used by cpp::comparison_operators */
  virtual bool is_equal_to(network const & other) const;

  /** Used by cpp::comparison_operators */
  virtual bool is_less_than(network const & other) const;

private:
  // Pointer to implementation
  struct network_impl;
  std::unique_ptr<network_impl> m_impl;

  // Creates a version of the given input address with the netmask applied.
  socket_address make_masked(socket_address const & input) const;

  friend LIBERATE_API_FRIEND std::ostream & operator<<(std::ostream & os, network const & net);
};


/**
 * Formats a network into human-readable/CIDR form.
 *
 * @param [in,out] os std::ostream to write to.
 * @param [in] net The network to output.
 *
 * @returns the passed std::ostream @p os.
 **/
LIBERATE_API std::ostream & operator<<(std::ostream & os, network const & net);


} // namespace liberate::net

#endif // guard
