/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2018-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_NET_URL_H
#define LIBERATE_NET_URL_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <string>
#include <map>

#include <liberate/cpp/operators/comparison.h>

namespace liberate::net {

/**
 * The URL class provides *minimal* URL parsing; this is no way specs
 * conforming.
 *
 * That is, more advanced features of proper URLs are ignored - such as
 * using URL escaping, or quoting, etc. The main goal is to split a URL
 * into:
 * - scheme
 * - authority (host)
 * - path
 * - query parameters
 * - fragment
 *
 * The logic is simple enough that, for example, the first # found is
 * assumed to be the fragment delimiter, etc. It also does not deal with
 * duplicate parameters; the last found always wins.
 *
 * One thing that it does, however, is lower-case the scheme, all query
 * parameters and their values, and convert all values indicating
 * boolean values to simple "0" and "1" (e.g. "true", "yes", etc.).
 */
struct LIBERATE_API url
  : public ::liberate::cpp::comparison_operators<url>
{
public:
  // Data members
  std::string                         scheme;     //!< Scheme
  std::string                         authority;  //!< Authority
  std::string                         path;       //!< Path
  std::map<std::string, std::string>  query;      //!< Key-value map of query
                                                  //!< parameters - note that
                                                  //!< this deduplicates
                                                  //!< parameters.
  std::string                         fragment;   //!< fragment

  /**
   * Parse string and return @ref url object.
   *
   * @param [in] url_string The string to parse.
   * @returns a parsed @ref url.
   */
  static url parse(char const * url_string);

  /** @copydoc parse */
  static url parse(std::string const & url_string);

  /**
   * Convert back to string.
   * @returns string representation of this object.
   */
  std::string str() const;

  /**
   * Swap values with another url.
   * @param [in,out] other The other object to swap values with.
   */
  void swap(url & other);

  /**
   * @returns a hash value for the object for use in unordered maps.
   */
  size_t hash() const;

private:
  friend struct liberate::cpp::comparison_operators<url>;

  bool is_less_than(url const & other) const;
  bool is_equal_to(url const & other) const;

  friend LIBERATE_API_FRIEND std::ostream & operator<<(std::ostream & os, url const & data);
};


/**
 * Formats a url into human-readable form.
 **/
LIBERATE_API std::ostream & operator<<(std::ostream & os, url const & data);

/**
 * Swappable
 **/
inline void
swap(url & first, url & second)
{
  return first.swap(second);
}

} // namespace liberate::net

/*******************************************************************************
 * std namespace specializations
 **/
namespace std {

/**
 * Specialization of `std::hash` for @ref liberate::net::url
 */
template <> struct LIBERATE_API hash<::liberate::net::url>
{
  /** Same as `std::hash` */
  size_t operator()(::liberate::net::url const & x) const
  {
    return x.hash();
  }
};

} // namespace std


#endif // guard
