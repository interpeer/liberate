/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_NET_RESOLVE_H
#define LIBERATE_NET_RESOLVE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <string>
#include <set>

#include <liberate/net/address_type.h>
#include <liberate/net/socket_address.h>

namespace liberate::net {

/**
 * Resolves network host names to IP addresses.
 * - specify AT_UNSPEC if you want both IPv4 and IPv6 addresses.
 * - specify AT_INET4 or AT_INET6 for only IPv4 or only IPv6 addresses
 *   respectively.
 * - any other address type will yield errors.
 *
 * An empty list means the name could not be resolved. Actual errors
 * are reported as exceptions.
 * - std::invalid_argument if arguments were invalid.
 * - std::domain_error if the functionality is not implemented.
 * - std::runtime_error if an error occurred in the underlying system
 *   call.
 * - std::range_error if the underlying system call returns
 *   unexpected information.
 * - std::logic_error for unspecified errors.
 */
LIBERATE_API
std::set<socket_address>
resolve(api & api, address_type type, std::string const & hostname);

} // namespace liberate::net

#endif // guard
