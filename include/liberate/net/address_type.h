/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_NET_ADDRESS_TYPE_H
#define LIBERATE_NET_ADDRESS_TYPE_H

// *** Config
#include <liberate.h>

namespace liberate::net {

/**
 * Socket or network address type.
 */
enum LIBERATE_API address_type : int8_t
{
  AT_UNSPEC = -1,     //!< Unspecified address/error
  AT_INET4 = 0,       //!< IPv4 address
  AT_INET6,           //!< IPv6 address
  AT_LOCAL,           //!< Only works for pipes, and not for networks.
  AT_UNIX = AT_LOCAL, //!< Alias for @ref AT_LOCAL for people who prefer that
                      //!< name.
};


} // namespace liberate::net

#endif // guard
