/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_NET_IP_H
#define LIBERATE_NET_IP_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <liberate/net/socket_address.h>
#include <liberate/types/byte.h>

namespace liberate::net {

/**
 * Given an byte buffer representing an IP packet, parses a socket_address from
 * the buffer. Returns true if an address could be parsed, false otherwise.
 *
 * This function does *not* verify that the packet is in fact a valid IP packet.
 * It merely treats it as such - it distinguishes between IPv4 and IPv6
 * protocols to determine which part of the buffer to read, but that is it.
 *
 * Consequently, if you feed it bad data, it will return a bad socket address.
 *
 * @param [out] source The parsed source address.
 * @param [out] dest The parsed destination address.
 * @param [in] buffer The buffer to read from; this is expected to be a valid
 *    IP packet (header).
 * @param [in] bufsize The size of the input buffer.
 * @returns true if parsing succeeded, false otherwise.
 */
LIBERATE_API bool
parse_addresses(socket_address & source, socket_address & dest,
    ::liberate::types::byte const * buffer, size_t bufsize);

/** Like @ref parse_addresses, but parse only the source address. */
LIBERATE_API bool
parse_source_address(socket_address & source,
    ::liberate::types::byte const * buffer, size_t bufsize);

/** Like @ref parse_addresses, but parse only the destination address. */
LIBERATE_API bool
parse_dest_address(socket_address & dest,
    ::liberate::types::byte const * buffer, size_t bufsize);


} // namespace liberate::net

#endif // guard
