/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2017-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_CPP_OPERATORS_COMPARISON_H
#define LIBERATE_CPP_OPERATORS_COMPARISON_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

namespace liberate::cpp {

/**
 * Supplement comparison operators when @p is_equal_to and @p is_less_than are
 * defined. Make sure to provide them as protected functions to allow class
 * hierarchies where every member uses this template.
 *
 * Use with curiously recurring inheritance.
 **/
template <typename T>
struct comparison_operators
{
  /** Standard comparison operator; uses @p T::is_equal_to */
  inline bool operator==(T const & other) const
  {
    return static_cast<T const *>(this)->T::is_equal_to(other);
  }

  /** Standard comparison operator; uses @p T::is_less_than */
  inline bool operator<(T const & other) const
  {
    return static_cast<T const *>(this)->T::is_less_than(other);
  }

  /** Standard comparison operator; uses other operators. */
  inline bool operator!=(T const & other) const
  {
    return !(*static_cast<T const *>(this) == other);
  }

  /** Standard comparison operator; uses other operators. */
  inline bool operator>(T const & other) const
  {
    return other < *static_cast<T const *>(this);
  }

  /** Standard comparison operator; uses other operators. */
  inline bool operator>=(T const & other) const
  {
    return !(*static_cast<T const *>(this) < other);
  }

  /** Standard comparison operator; uses other operators. */
  inline bool operator<=(T const & other) const
  {
    return !(other < *static_cast<T const *>(this));
  }
};

} // namespace liberate::cpp

/**
 * Supplement comparison operators as free functions for types that already
 * define == and <.
 */
#define LIBERATE_MAKE_COMPARABLE(thetype) \
    inline bool operator!=(thetype const & first, thetype const & second) \
    { \
      return !(first == second); \
    } \
    inline bool operator>(thetype const & first, thetype const & second) \
    { \
      return second < first; \
    } \
    inline bool operator>=(thetype const & first, thetype const & second) \
    { \
      return !(first < second); \
    } \
    inline bool operator<=(thetype const & first, thetype const & second) \
    { \
      return !(second < first); \
    }


#endif // guard
