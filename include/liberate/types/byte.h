/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_TYPES_BYTE_H
#define LIBERATE_TYPES_BYTE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <cstddef>
#include <vector>

namespace liberate::types {

#if defined(LIBERATE_HAVE_STD_BYTE)
using byte = std::byte;
#else
using byte = uint8_t;
#endif // LIBRATE_HAVE_STD_BYTE

namespace literals {


/**
 * Integer and character literals are converted to bytes. Note that for longer
 * integer literals, they are truncated by the cast, effectively resulting in
 * (val % 256).
 *
 * String literals are converted to vectors of bytes.
 */
inline constexpr byte operator ""_b(unsigned long long arg) noexcept
{
  return static_cast<byte>(arg);
}


inline constexpr byte operator ""_b(char arg) noexcept
{
  return static_cast<byte>(arg);
}


inline std::vector<byte> operator ""_b(char const * str, std::size_t len) noexcept
{
  auto start = reinterpret_cast<byte const *>(str);
  return {start, start + len};
}


} // namespace literals

} // namespace liberate::types

#endif //guard
