/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_TYPES_TYPE_TRAITS_H
#define LIBERATE_TYPES_TYPE_TRAITS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <type_traits>

namespace liberate::types {

/**
 * An integral constant that is either equivalent to std::true_type or
 * std::false_type, depending on whether the byte size of the given type
 * is the given value.
 */
template <typename T, std::size_t BYTE_SIZE>
using has_byte_size = std::integral_constant<
  bool,
  sizeof(T) == BYTE_SIZE
>;


/**
 * Determine whether the type has 8, 16, 32, 64 or 128 bits in
 * size. There is not much use for other sizes at the moment.
 */
template <typename T>
struct is_8bit_type
  : public has_byte_size<
      typename std::remove_cv<T>::type,
      1
    >::type
{};

template <typename T>
struct is_16bit_type
  : public has_byte_size<
      typename std::remove_cv<T>::type,
      2
    >::type
{};

template <typename T>
struct is_32bit_type
  : public has_byte_size<
      typename std::remove_cv<T>::type,
      4
    >::type
{};

template <typename T>
struct is_64bit_type
  : public has_byte_size<
      typename std::remove_cv<T>::type,
      8
    >::type
{};

template <typename T>
struct is_128bit_type
  : public has_byte_size<
      typename std::remove_cv<T>::type,
      16
    >::type
{};


} // namespace liberate::types

#endif // guard
