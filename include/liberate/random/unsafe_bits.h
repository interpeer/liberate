/*
 * This file is part of liberate.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBERATE_RANDOM_UNSAFE_BITS_H
#define LIBERATE_RANDOM_UNSAFE_BITS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate.h>

#include <random>
#include <chrono>
#include <limits>
#include <type_traits>

namespace liberate::random { // flawfinder: ignore

/**
 * Return a random value of type T.
 *
 * The structure is called "unsafe_bits" because it does not produce random
 * bits suitable for cryptographic applications.
 *
 * @param [in] T The (integer) type to generate bits for.
 */
template <typename T>
struct unsafe_bits
{
  //! Internal generator object
  std::default_random_engine generator;

  /**
   * For any integer smaller than 2 Bytes in size, we have to produce values
   * from a 2 Byte type with the same sign. Here, we're selecting the right
   * type.
   *
   * This construct selects the matching output type for the uniform
   * distribution.
   */
  using generator_type = typename std::conditional<
    sizeof(T) == 1,
    // Signed or unsigned 16 bit
    typename std::conditional<
      std::is_signed<T>::value,
      int16_t,
      uint16_t
    >::type,
    // T is fine to use directly
    T
  >::type;

  //! Internal uniform distribution object.
  std::uniform_int_distribution<generator_type> distribution;

  /**
   * Create an instance of a generator.
   *
   * @param [in] min optional minimum value; defaults to the @p T's minimum
   *    value.
   * @param [in] max optional maximum value; defaults to the @p T's maximum
   *    value.
   */
  inline explicit unsafe_bits(
      T const & min = std::numeric_limits<T>::min(),
      T const & max = std::numeric_limits<T>::max())
    : generator{
        static_cast<std::default_random_engine::result_type>(
            std::chrono::system_clock::now().time_since_epoch().count()
        )
      }
    , distribution{min, max}
  {
  }

  /**
   * @returns a random value of type @p T that is between the minimum and
   *  maximum specified in the constructor.
   */
  inline T get()
  {
    return distribution(generator);
  }

  /**
   * @returns a random value as a factor. This treats the range of values
   *   given as the minimum and maximum in the constructor as [0,1] and
   *   returns a value between the two. The implication is that the maximum
   *   and minimum mostly describe the number of discrete values this function
   *   can produce.
   */
  inline double get_factor()
  {
    return static_cast<long double>(get()) /
      (std::numeric_limits<T>::max() - std::numeric_limits<T>::min());
  }
};


} // namespace liberate::random

#endif // guard
