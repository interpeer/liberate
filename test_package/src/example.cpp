#include <iostream>
#include <liberate/version.h>

int main()
{
  std::cout << liberate::copyright_string() << std::endl;
}
